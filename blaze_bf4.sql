-- phpMyAdmin SQL Dump
-- version 4.2.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2016 at 09:50 PM
-- Server version: 5.5.40-0+wheezy1
-- PHP Version: 5.4.45-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blaze_bf4`
--

-- --------------------------------------------------------

--
-- Table structure for table `game_reports`
--

CREATE TABLE IF NOT EXISTS `game_reports` (
`ReportID` int(12) NOT NULL,
  `GameID` int(12) NOT NULL,
  `PersistentGameID` varchar(64) NOT NULL,
  `GameType` varchar(64) NOT NULL,
  `IsFinished` int(1) NOT NULL DEFAULT '0',
  `TimeUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `global_entitlements`
--

CREATE TABLE IF NOT EXISTS `global_entitlements` (
`EntitlementID` int(12) NOT NULL,
  `GroupName` varchar(32) NOT NULL,
  `ProductID` varchar(32) NOT NULL,
  `Tag` varchar(32) NOT NULL,
  `Type` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_entitlements`
--

INSERT INTO `global_entitlements` (`EntitlementID`, `GroupName`, `ProductID`, `Tag`, `Type`) VALUES
(1, 'BF4PC', 'DR:235663400', 'ONLINE_ACCESS', 1),
(2, 'BF4PC', '', 'PREMIUM_ACCESS', 5),
(3, 'BF4PC', '', 'XPACK0_ACCESS', 5),
(4, 'BF4PC', '', 'XPACK1_ACCESS', 5),
(5, 'BF4PC', '', 'XPACK2_ACCESS', 5),
(6, 'BF4PC', '', 'XPACK3_ACCESS', 5),
(7, 'BF4PC', '', 'XPACK4_ACCESS', 5),
(8, 'BF4PC', '', 'XPACK5_ACCESS', 5),
(9, 'BF4PC', '', 'XPACK6_ACCESS', 5),
(10, 'BF4PC', '', 'XPACK7_ACCESS', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`UserID` int(12) NOT NULL,
  `BlazeUserID` int(12) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '2',
  `LastLogin` int(12) NOT NULL DEFAULT '0',
  `FirstLogin` int(1) NOT NULL DEFAULT '1',
  `UserAttribute` bigint(30) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_entitlements`
--

CREATE TABLE IF NOT EXISTS `user_entitlements` (
`EntitlementID` int(12) NOT NULL,
  `UserID` int(12) NOT NULL,
  `GroupName` varchar(32) NOT NULL,
  `ProductID` varchar(32) NOT NULL,
  `Tag` varchar(32) NOT NULL,
  `Type` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blaze_api_keys`
--
ALTER TABLE `blaze_api_keys`
 ADD PRIMARY KEY (`api_id`), ADD UNIQUE KEY `api_key` (`api_key`,`owner_buid`);

--
-- Indexes for table `game_reports`
--
ALTER TABLE `game_reports`
 ADD PRIMARY KEY (`ReportID`);

--
-- Indexes for table `global_entitlements`
--
ALTER TABLE `global_entitlements`
 ADD PRIMARY KEY (`EntitlementID`);

--
-- Indexes for table `server_users`
--
ALTER TABLE `server_users`
 ADD PRIMARY KEY (`sUserID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `user_entitlements`
--
ALTER TABLE `user_entitlements`
 ADD PRIMARY KEY (`EntitlementID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blaze_api_keys`
--
ALTER TABLE `blaze_api_keys`
MODIFY `api_id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `game_reports`
--
ALTER TABLE `game_reports`
MODIFY `ReportID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=364;
--
-- AUTO_INCREMENT for table `global_entitlements`
--
ALTER TABLE `global_entitlements`
MODIFY `EntitlementID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `server_users`
--
ALTER TABLE `server_users`
MODIFY `sUserID` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `UserID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_entitlements`
--
ALTER TABLE `user_entitlements`
MODIFY `EntitlementID` int(12) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
