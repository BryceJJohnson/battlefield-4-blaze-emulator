// Protocol Encoder
// Decodes Blaze protocol messages to a readable format
var Enums 	= require('./Enums');
var bigInt 	= require("big-integer");

function encodeInt(value) {
	result = [];
	if (value <= 0x3F) {
		result.push(value);
	} else {
		curbyte = bigInt(value).and(0x3F).or(0x80);
		result.push(curbyte.toJSNumber());
		currshift = bigInt(value).shiftRight(6);
		
		while (currshift.greaterOrEquals(0x80)) {
			curbyte = currshift.and(0x7F).or(0x80);
			currshift = currshift.shiftRight(7)
			result.push(curbyte.toJSNumber());
		}
		result.push(currshift.toJSNumber());
	}
	
	for (i = 0; i < result.length; i++) {
		result[i] = result[i].toString(16);
		
		if (result[i].length == 1)
			result[i] = "0"+result[i];
	}
	
	if (result[0] == "00" && result.length != 1)
		result.pop(0);
	result = result.join("");
	
	return result.replace(/[^a-zA-Z0-9]/g, '');
}

function a2hex(str) {
	var arr = [];
	for (var i = 0, l = str.length; i < l; i ++) {
		var hex = Number(str.charCodeAt(i)).toString(16);
		arr.push(hex);
	}
	return arr.join('');
}
function hex_to_ascii(str1)  
 {  
    var hex  = str1.toString();  
    var str = '';  
    for (var n = 0; n < hex.length; n += 2) {  
        str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));  
    }  
    return str;  
 } 
function writeTag(tag) {
	tag = tag.match(/.{1,1}/g);
	var buffer = 0x00
	
	while (tag.length < 4)
		tag.push(" ")

	for (i = 0; i < 4; i++)
		tag[i] = a2hex(tag[i]);
	
	test = (parseInt(tag[0], 16) - parseInt('20',16))
	buffer += (test << 18)
		
	test = (parseInt(tag[1], 16) - parseInt('20',16))
	buffer += (test << 12)

	test = (parseInt(tag[2], 16) - parseInt('20',16))
	buffer += (test << 6)
		
	test = (parseInt(tag[3], 16) - parseInt('20',16))
	buffer += test
	
	return buffer.toString(16);
}

//Write functions for external classes
BlazeEncoder.prototype.writeInteger = function(tag, value) {
	this.packet_data[tag] = [Enums.TagType.TYPE_INTEGER, value];
}
BlazeEncoder.prototype.writeString = function(tag, string) {
	this.packet_data[tag] = [Enums.TagType.TYPE_STRING, string];
}
BlazeEncoder.prototype.writeBlob = function(tag, data) {
	this.packet_data[tag] = [Enums.TagType.TYPE_BLOB, data];
}
BlazeEncoder.prototype.writeStruct = function(tag, data) {
	this.packet_data[tag] = [Enums.TagType.TYPE_STRUCT, data];
}
BlazeEncoder.prototype.writeList = function(tag, data, type) {
	this.packet_data[tag] = [Enums.TagType.TYPE_LIST, data, type];
}
BlazeEncoder.prototype.writeMap = function(tag, data, type1, type2) {
	this.packet_data[tag] = [Enums.TagType.TYPE_MAP, data, type1, type2];
}
BlazeEncoder.prototype.writeUnion = function(tag, data, type) {
	this.packet_data[tag] = [Enums.TagType.TYPE_UNION, data, type];
}
BlazeEncoder.prototype.writeIntegerList = function(tag, data) {
	this.packet_data[tag] = [Enums.TagType.TYPE_INTLIST, data];
}
BlazeEncoder.prototype.writeVector3 = function(tag, data) {
	this.packet_data[tag] = [Enums.TagType.TYPE_VECTOR2, data];
}
BlazeEncoder.prototype.writeVector3 = function(tag, data) {
	this.packet_data[tag] = [Enums.TagType.TYPE_VECTOR3, data];
}
BlazeEncoder.prototype.writeAppend = function(tag, data) {
	this.packet_data[tag] = [Enums.TagType.TYPE_APPEND, data];
}

//Build functions for internal building of hex string
BlazeEncoder.prototype.buildInteger = function (key, value) {
	var tag = writeTag(key);
	var value = encodeInt(value);
	
	return tag+"00"+value;
}
BlazeEncoder.prototype.buildString = function (key, value) {
	var tag = writeTag(key);
	var length = encodeInt(value.length+1);
	var value = a2hex(value) + "00";
	
	return tag+"01"+length+value;
}
BlazeEncoder.prototype.buildBlob = function (key, value) {
	var tag = writeTag(key);
	var length = encodeInt(value.length/2);
	
	return tag+"02"+length+value;
}
BlazeEncoder.prototype.buildStruct = function (key, value) {
	var tag 			= writeTag(key)+"03";
	var buffer = tag;
	
	for (key2 in value) {
		var type = value[key2][0];
		var data = value[key2][1];
		
		if(type == Enums.TagType.TYPE_UNION) {
			buffer += this.buildValue(type, key2, data, value[key2][2]);
		} else if(type == Enums.TagType.TYPE_MAP) {
			buffer += this.buildValue(type, key2, data, value[key2][2], value[key2][3]);
		} else if(type == Enums.TagType.TYPE_LIST) {
			buffer += this.buildValue(type, key2, data, value[key2][2]);
		} else {
			buffer += this.buildValue(type, key2, data);
		}
	}
	
	return buffer += "00";
}
BlazeEncoder.prototype.buildList = function (key, data, listType) {
	var tag 		= writeTag(key);
	var dataLength 	= 0;
	var buff 		= "";
	var snub		= false;
	
	if(key == "HNET" || key == "PNET")
		snub = true;
	if(listType === undefined)
		listType = 0x1
	
	if(listType == Enums.TagType.TYPE_INTEGER) {
		for(value in data) {
			buff += encodeInt(data[value]);
			
			dataLength += 1;
		}
	} else if(listType == Enums.TagType.TYPE_STRING) {
		for(value in data) {
			buff += encodeInt(data[value].length+1);
			buff += a2hex(data[value]) + "00";
			
			dataLength += 1;
		}
	} else if(listType == Enums.TagType.TYPE_STRUCT) {
		for(var index in data) {
			var tmp = "";
			if(snub) {
				tmp = this.buildValue(data[index][0], index, data[index][1]);
			} else {
				for(value in data[index]) {		
					tmp += this.buildValue(data[index][value][0], value, data[index][value][1]);
				}
				if(data.length > 1)
					tmp += "00";
			}
			buff += tmp;
			dataLength++;
		}
		if(dataLength > 0)
			buff += "00";
	} else if(listType == Enums.TagType.TYPE_VECTOR3) {
		for(value in data) {
			for (var number in data[value]) {
				buff += encodeInt(data[value][number]);
			}
			dataLength++;
		}
	}
	
	dataLength = encodeInt(dataLength);
	
	while(dataLength.length % 2 == 1)
		dataLength = "0"+dataLength;
	
	while(listType.toString(16).length % 2 == 1)
		listType = "0"+listType;
	
	var header = "";
	if(snub)
		header = tag+"04"+listType+"0102"
	else 
		header = tag+"04"+listType+dataLength
	
	return header+buff;
}
BlazeEncoder.prototype.buildMap = function (key, data, type1, type2) {
	var tag 		= writeTag(key);
	var dataLength 	= 0;
	var buff 		= "";
	
	while(dataLength.length < 2)
		dataLength = "0"+dataLength;
	
	if(type1 === undefined)
		type1 = "01";
	if(type2 === undefined)
		type2 = "01"
	
	if(key == "RMAP")
		type2 = "00"
	
	for(key in data) {
		
		//KEY
		if(type1 == Enums.TagType.TYPE_STRING) {
			buff += encodeInt(key.length+1);
			buff += a2hex(key) + "00";
		} else if(type2 == Enums.TagType.TYPE_INTEGER) {
			buff += encodeInt(key);
		}
		
		//VALUE
		if(type2 == Enums.TagType.TYPE_STRING) {
			buff += encodeInt(data[key].length+1);
			buff += a2hex(data[key]) + "00";
		} else if(type2 == Enums.TagType.TYPE_INTEGER) {
			buff += encodeInt(data[key]);
		} else if(type2 == Enums.TagType.TYPE_STRUCT) {
			for(val in data[key]) {
				buff += this.buildValue(data[key][val][0], val, data[key][val][1])
			}
			buff += "00";
		}
		
		dataLength++;
	}
	
	while(type1.toString(16).length < 2)
		type1 = "0"+type1;
	while(type2.toString(16).length < 2)
		type2 = "0"+type2;

	dataLength = encodeInt(dataLength);
	
	return tag+"05"+type1+type2+dataLength+buff;
}
BlazeEncoder.prototype.buildUnion = function (key, value, unionType) {
	var tag = writeTag(key)+"06";
	var buffer = tag;
	unionType = encodeInt(unionType);
	
	while(unionType.length < 2)
		unionType = unionType+"0";
	
	//console.log("Union TYPE = " + unionType);
	// Fix hacky way
	if(unionType == "8000")
		unionType = "02"
	
	buffer += unionType
	
	for (var key2 in value) {
		var type = value[key2][0];
		var data = value[key2][1];
		
		if(type == Enums.TagType.TYPE_UNION) {
			buffer += this.buildValue(type, key2, data, value[key2][2]);
		} else {
			buffer += this.buildValue(type, key2, data);
		}
	}
	
	return buffer;
}
BlazeEncoder.prototype.buildIntegerList = function (key, value) {
	var buffer = writeTag(key)+"07";
	for (var number in value) {
		buffer += encodeInt(value[number]);
	}
	return buffer;
}
BlazeEncoder.prototype.buildVector2 = function (key, value) {
	var buffer = writeTag(key)+"08";
	for (var number in value) {
		buffer += encodeInt(value[number]);
	}
	return buffer;
}
BlazeEncoder.prototype.buildVector3 = function (key, value) {
	var buffer = writeTag(key)+"09";
	for (var number in value) {
		buffer += encodeInt(value[number]);
	}
	return buffer;
}
BlazeEncoder.prototype.buildAppend = function (key, value) {
	return value;
}

//Builder Switch
BlazeEncoder.prototype.buildValue = function(type, key, data, extra, extra2) {
	switch(type) {
		case Enums.TagType.TYPE_INTEGER:
			return this.buildInteger(key, data);
		case Enums.TagType.TYPE_STRING:
			return this.buildString(key, data);
		case Enums.TagType.TYPE_BLOB:
			return this.buildBlob(key, data);
		case Enums.TagType.TYPE_STRUCT:
			return this.buildStruct(key, data);
		case Enums.TagType.TYPE_LIST:
			return this.buildList(key, data, extra);
		case Enums.TagType.TYPE_MAP:
			return this.buildMap(key, data, extra, extra2);
		case Enums.TagType.TYPE_UNION:
			return this.buildUnion(key, data, extra);
		case Enums.TagType.TYPE_INTLIST:
			return this.buildIntegerList(key, data);
		case Enums.TagType.TYPE_VECTOR2:
			return this.buildVector2(key, data);
		case Enums.TagType.TYPE_VECTOR3:
			return this.buildVector3(key, data);
		case Enums.TagType.TYPE_APPEND:
			return this.buildAppend(key, data);
		default:
			console.log("UNKNOWN TAG TYPE " + type);
	}
}

BlazeEncoder.prototype.buildPacket = function() {
	for (key in this.packet_data) {
		var type = this.packet_data[key][0];
		var data = this.packet_data[key][1];
		
		if(type == Enums.TagType.TYPE_UNION) {
			this.packet_content += this.buildValue(type, key, data, this.packet_data[key][2]);
		} else if(type == Enums.TagType.TYPE_MAP) {
			this.packet_content += this.buildValue(type, key, data, this.packet_data[key][2], this.packet_data[key][3]);
		} else if(type == Enums.TagType.TYPE_LIST) {
			this.packet_content += this.buildValue(type, key, data, this.packet_data[key][2]);
		} else {
			this.packet_content += this.buildValue(type, key, data);
		}
	}
	
	this.packet_contentSize = this.packet_content.length/2;
	var encodedSize 		= this.packet_contentSize.toString(16);
	var packComponent 		= this.packet_component.toString(16);
	var packCommand 		= this.packet_command.toString(16);
	var packError	 		= this.packet_error.toString(16);
	var packType	 		= this.packet_type.toString(16);
	var packId		 		= this.packet_id.toString(16);
	
	var sizeTwo				= "";
	
	
	//Hex Size Adjustment
	while(encodedSize.length < 4)
		encodedSize = "0"+encodedSize;
	while(packComponent.length < 4)
		packComponent = "0"+packComponent;
	while(packCommand.length < 4)
		packCommand = "0"+packCommand;
	while(packError.length < 4)
		packError = "0"+packError;
	while(packType.length < 4)
		packType = packType+"0";
	while(packId.length < 4)
		packId = "0"+packId;
	
	if(this.isBlazeClient)
		packType = "00100000";
	
	if (this.packet_contentSize > 0xFFFF) {
		var size1 = encodedSize.substr(encodedSize.length-4);
		var size2 = encodedSize.substr(0, encodedSize.length-4);
		
		while(size2.length < 4)
			size2 = "0"+size2;
		
		encodedSize = size1;
		sizeTwo 	= size2;
	}
	
	this.packet_header 	= encodedSize + packComponent + packCommand + packError + packType + packId + sizeTwo;
	this.packet_size	= this.packet_contentSize + (this.packet_header.length/2);
	
	this.packet_complete = (this.packet_header+this.packet_content);
	return true;
}

function BlazeEncoder(component, command, error, type, id, isBlazeClient) {
	if(isBlazeClient === undefined)
		isBlazeClient = false;
	
	this.packet_size		= 0
	this.packet_contentSize = 0;
	this.packet_header 		= "";
	
	this.packet_component 	= component;
	this.packet_command 	= command;
	this.packet_error 		= error;
	this.packet_type 		= type;
	this.packet_id 			= id;

	this.isBlazeClient 		= isBlazeClient;
	
	this.packet_data 		= [];
	this.packet_content		= ""
	this.packet_complete	= "";
}

module.exports = BlazeEncoder;