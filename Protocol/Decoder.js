// Protocol Decoder
// Decodes Blaze protocol messages to a readable format
var Enums 		= require('./Enums');
var bigInt 		= require("big-integer");
var binStruct 	= require('binstruct');

function decodeInt(value) {
	
	var fuckingNumberFuckedUpTheArse = 0;
	value = String(value);
	if (value.slice(-2) == "00")
		value = value.slice(-2);
	
	if (value.length == 0)
		return 0;

	value = value.match(/.{1,2}/g);
	
	fuckingNumberFuckedUpTheArse = bigInt(parseInt(value[0],16)).and(0x3F);
	for (i = 1; i < value.length; i++) {
		curByte = bigInt(parseInt(value[i], 16)).and(0x7F).shiftLeft((i * 7) - 1);
		fuckingNumberFuckedUpTheArse = fuckingNumberFuckedUpTheArse.or(curByte);
	}

	return fuckingNumberFuckedUpTheArse.toJSNumber()
}

function hex2a(hex) {
    var str = '';
    for (var i = 0; i < hex.length; i += 2) {
        var v = parseInt(hex.substr(i, 2), 16);
        if (v) str += String.fromCharCode(v);
    }
    return str;
}
function readTag(tag) {
	tag = tag.match(/.{1,2}/g);
	var label = "";
	var buff = [];

	for (i = 0; i < 3; i++) {
		buff.push(parseInt(tag[i], 16));
	}
	
	var res = [0, 0, 0, 0];
	res[0] |= ((buff[0] & 0x80) >> 1);
	res[0] |= ((buff[0] & 0x40) >> 2);
	res[0] |= ((buff[0] & 0x30) >> 2);
	res[0] |= ((buff[0] & 0x0C) >> 2);
	
	res[1] |= ((buff[0] & 0x02) << 5);
	res[1] |= ((buff[0] & 0x01) << 4);
	res[1] |= ((buff[1] & 0xF0) >> 4);
	
	res[2] |= ((buff[1] & 0x08) << 3);
	res[2] |= ((buff[1] & 0x04) << 2);
	res[2] |= ((buff[1] & 0x03) << 2);
	res[2] |= ((buff[2] & 0xC0) >> 6);
	
	res[3] |= ((buff[2] & 0x20) << 1);
	res[3] |= ((buff[2] & 0x1F));
	
	for (i = 0; i < 4; i++) {
		if (res[i] == 0)
			res[i] = 0x20;
		label += hex2a(res[i].toString(16));
	}

	return label;
}

//Read functions
BlazeDecoder.prototype.readInteger = function() {
	var buff = "";
	var value = false

	
	while (!value) {
		var hex = this.packet_data.substring(this.packet_position, this.packet_position+2);
		var num = parseInt(hex, 16);
		
		this.packet_position += 2;
		
		buff += hex;
		
		if(num < 0x80)
			return decodeInt(buff);
		
		continue;
	}
	
	return 0;
}
BlazeDecoder.prototype.readString = function() {
	var length = this.readInteger()*2;
	
	var string = this.packet_data.substring(this.packet_position, this.packet_position+length-1);
	string = hex2a(string.substring(0, string.length));
	this.packet_position += length;

	return string;
}
BlazeDecoder.prototype.readBlob = function() {
	var length 	= this.readInteger()*2;
	var blob 	= this.packet_data.substring(this.packet_position, this.packet_position+length);
	
	return blob;
}
BlazeDecoder.prototype.readStruct = function() {
	var isFinished = parseInt(this.packet_data.substring(this.packet_position, this.packet_position+2), 16);
	var values = [];
	while(isFinished != 0) {
		var tag = readTag(this.packet_data.substring(this.packet_position, this.packet_position+6));
		this.packet_position += 6;
	
		var type = parseInt(this.packet_data.substring(this.packet_position, this.packet_position+2), 16);
		this.packet_position += 2;
		
		if(type == Enums.TagType.TYPE_MAP) {
			var value = this.readValue(type);
			values[tag] = [type, value[0], value[1][0], value[1][1]];
		} else {
			values[tag] = [type, this.readValue(type)];
		}
		
		
		isFinished = parseInt(this.packet_data.substring(this.packet_position, this.packet_position+2), 16);
	}
	this.packet_position += 2;

	return values;
}
BlazeDecoder.prototype.readList = function() {
	var listType 	= this.readInteger();
	var amount 		= this.readInteger();
	var values = [];
	var curVal = 0;
	
	//HNET
	if (parseInt(this.packet_data.substring(this.packet_position, this.packet_position+2), 16) == 0x02 && listType == Enums.TagType.TYPE_STRUCT) {
		this.packet_position += 2;
			
		var exip = this.readPacket();
		var inip = this.readPacket();
		var maci = this.readPacket();
		
		values[exip.label] = [exip.type, exip.data];
		values[inip.label] = [inip.type, inip.data];
		values[maci.label] = [maci.type, maci.data];
		
		this.packet_position += 2;
	} else {	
		while(curVal < amount) {
			var val = this.readValue(listType);
			values.push(val);
			curVal++;
		}
	}
	
	return values;
}
BlazeDecoder.prototype.readMap = function() {
	var type1 =	this.readInteger();
	var type2 = this.readInteger();
	var amount = this.readInteger();
	
	var curVal = 0;
	var values = [];
	
	do {
		var key = this.readValue(type1);	
		var val = this.readValue(type2);
		
		if(typeof key === "number")
			key = "_"+key
		
		values[key] = val;
		curVal++;
	} while(curVal < amount);
	
	var typeMap = [type1, type2];

	return [values, typeMap];
}
BlazeDecoder.prototype.readUnion = function() {
	var addressMember = parseInt(this.packet_data.substring(this.packet_position, this.packet_position+2), 16)
	this.packet_position += 2;
	var values = [];
	
	if (addressMember != Enums.NetworkAddressMember.MEMBER_UNSET) {
		var value = this.readPacket();
		values[value.label] = [value.type, value.data];
	}
	
	return values;
}
BlazeDecoder.prototype.readIntegerList = function() {
	var amount = this.readInteger();
	var values = [];
	
	for(var i = 0; i < amount; i++)
		values.push(this.readInteger());
	return values;
}
BlazeDecoder.prototype.readVector2 = function() {
	var value1 = this.readInteger();
	var value2 = this.readInteger();
	
	return [value1, value2];
}
BlazeDecoder.prototype.readVector3 = function() {
	var value1 = this.readInteger();
	var value2 = this.readInteger();
	var value3 = this.readInteger();
	
	return [value1, value2, value3];
}
BlazeDecoder.prototype.readFloat = function() {
	var hexString = this.packet_data.substring(this.packet_position, this.packet_position+8)
	this.packet_position += 8;
	
	return binStruct.def().float("a").read(new Buffer(hexString, 'hex')).a;
}

//Reader Switch
BlazeDecoder.prototype.readValue = function(type) {
	switch(type) {
		case Enums.TagType.TYPE_INTEGER:
			return this.readInteger();
		case Enums.TagType.TYPE_STRING:
			return this.readString()
		case Enums.TagType.TYPE_BLOB:
			return this.readBlob();
		case Enums.TagType.TYPE_STRUCT:
			return this.readStruct();
		case Enums.TagType.TYPE_LIST:
			return this.readList();
		case Enums.TagType.TYPE_MAP:
			return this.readMap();
		case Enums.TagType.TYPE_UNION:
			return this.readUnion();
		case Enums.TagType.TYPE_INTLIST:
			return this.readIntegerList();
		case Enums.TagType.TYPE_VECTOR2:
			return this.readVector2();
		case Enums.TagType.TYPE_VECTOR3:
			return this.readVector3();
		case Enums.TagType.TYPE_FLOAT:
			return this.readFloat();
		default:
			break;
	}
}

BlazeDecoder.prototype.readPacket = function() {
	var tag = readTag(this.packet_data.substring(this.packet_position, this.packet_position+6));
	this.packet_position += 6;
	
	var type = parseInt(this.packet_data.substring(this.packet_position, this.packet_position+2), 16);
	this.packet_position += 2;
	
	var value = this.readValue(type);
	
	//TODO FIGURE OUT THIS FUCKING MAP -- LOOKS BUTT UGLAY
	if(tag == "RRST") {
		this.packet_position += 2;
		this.readInteger();
	}
	
	var tagData = {
		label: tag,
		type: type,
		data: value
	}
	
	if(type == Enums.TagType.TYPE_MAP) {
		tagData.map_info = tagData.data[1];
		tagData.data = tagData.data[0];
	}
		
	return tagData;
}

BlazeDecoder.prototype.decodePacket = function() {
	while (this.packet_position < this.packet_contentSize) {
		var tagData = this.readPacket();
		
		if (tagData != 0) {
			
			if(tagData.type == Enums.TagType.TYPE_MAP) {
				this.packet_decoded[tagData.label] = [tagData.type, tagData.data, tagData.map_info[0], tagData.map_info[1]];
			} else {
				this.packet_decoded[tagData.label] = [tagData.type, tagData.data];
			}
		}
	}
}

function BlazeDecoder(packet) {
	
	this.packet_header 	= packet.substring(0, 24);
	this.packet_data 	= packet.substring(24, packet.length);

	this.packet_size			= (parseInt(this.packet_header.substring(0, 4), 16)*2)+24;
	this.packet_contentSize 	= (parseInt(this.packet_header.substring(0, 4), 16)*2);
	this.packet_component 		= parseInt(this.packet_header.substring(4, 8), 16);
	this.packet_command 		= parseInt(this.packet_header.substring(8, 12), 16);
	this.packet_error 			= parseInt(this.packet_header.substring(12, 16), 16);
	this.packet_type 			= parseInt(this.packet_header.substring(16, 20), 16);
	this.packet_id 				= parseInt(this.packet_header.substring(20, 24), 16);
	this.packet_contentSize2	= 0;
	this.packet_position		= 0;
	
	//Additional Packet Size for specific headers
	if(this.packet_type == 0x10) {
		this.packet_header 	= packet.substring(0, 28);
		this.packet_data 	= packet.substring(28, packet.length);
		
		this.packet_contentSize 	= parseInt((this.packet_header.substring(24, 28)+this.packet_header.substring(0, 4)), 16)*2;
		this.packet_size 			= this.packet_contentSize+28;
	}
	
	this.packet_decoded = [];
	
	this.decodePacket();
}

module.exports = BlazeDecoder;