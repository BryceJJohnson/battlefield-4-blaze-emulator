// BlazeUtils
// Handles Utility Functions

var Enums 			= require('./Enums');
var fs 				= require('fs-extra')
var path  			= require("path");

module.exports = {
	getCurrentUnixTime: function() {
		return Math.floor(new Date() / 1000);
	},
	
	getGameNameFromType: function(gameType) {
		if(gameType == "warsaw client" || gameType == "warsaw server") {
			return "battlefield-4";
		} else if(gameType == "venice client" || gameType == "venice server") {
			return "battlefield-3";
		}
	},
	
	createEmptyFileIfNotExistJson: function(filePath) {
		var fileExists 	= fs.existsSync(filePath);
		if(!fileExists) {
			fs.writeJson(filePath, []);
		}
	},
	
	createEmptyFileIfNotExist: function(filePath) {
		var fileExists 	= fs.existsSync(filePath);
		if(!fileExists) {
			fs.createWriteStream(filePath, {flags: 'w', encoding: 'utf-8',mode: 0777});
		}
	},
	
	createDirIfNotExists: function(directory, callback) {  
		fs.stat(directory, function(err, stats) {
			//Check if error defined and the error code is "not exists"
			if (err && err.errno === 34) {
			//Create the directory, call the callback.
				fs.mkdir(directory, callback);
			} else {
				//just in case there was a different error:
				callback(err)
			}
		});
	}
}