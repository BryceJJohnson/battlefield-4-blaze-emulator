var fs 		= require('fs');
var Config 	= require('./Config');

Config.setServerIPInteger();

console.log("Blaze IP: " + Config.ServerIP)
 
var options = {
    key: fs.readFileSync('BlazeData/ssl/key.pem'),
    cert: fs.readFileSync('BlazeData/ssl/cert.pem')
};

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';


//Https
var https 		= require('./Scripts/clientAuthServer');

//Redirector
var redirector 	= require('./Scripts/socketServer')(options, 42127, true);

//Blaze Master
var blazeHub 	= require('./Scripts/socketServer')(options, 10041, false);