// Utility Component
// Handles utility packets
var fs = require('fs');

var id 		= 0x0007;
var name 	= "Stats";

var commands = []
var normalizedPath = require("path").join(__dirname, "../Commands/"+name);
require("fs").readdirSync(normalizedPath).forEach(function(file) {
	commands.push(require("../Commands/" + name + "/" + file));
});

module.exports = {
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	handlePacket: function(socket, blazePacket) {
		for (i = 0; i < commands.length; i++) {
			if(commands[i].getId() == blazePacket.packet_command) {
				console.log("PARSING "+name+"::"+commands[i].getName()+" [" + id.toString(16) + "::" + commands[i].getId().toString(16) + "]");
				commands[i].createResponse(socket, blazePacket);
				return true;
			}
		}
		
		console.log("UNKNOWN [" + name + "::" + blazePacket.packet_command.toString(16) + "]");
		return false;
	}
};