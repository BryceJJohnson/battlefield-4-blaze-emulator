// databaseManager
// Manages the MySQL Database connection and the queries.

var mysql	= require('mysql');
var Q		= require("q");

var Config 	= require('../Config');

var connection = mysql.createConnection({
	host     : Config.MySQL_Host,
	user     : Config.MySQL_User,
	password : Config.MySQL_Pass,
	database : Config.MySQL_DB
});

connection.connect(function(err){
	if(!err) {
		console.log("Connected to MySQL Server ...");    
	} else {
		console.log("Error connecting to MySQL ...");    
	}
});

module.exports = {
	//GameReporting Functions
	////////////////////////////////////
	setupNewGameReport: function(gameid, persistentid, gametype) {
		var deferred = Q.defer();
		connection.query('INSERT INTO game_reports (`GameID`, `PersistentGameID`, `GameType`) VALUES (?, ?, ?);', [gameid, persistentid, gametype], function(err, rows, fields) {
			if (!err) {
				deferred.resolve(rows.insertId);
			} else {
				deferred.resolve(false);
			}
		});
		
		return deferred.promise;
	},
	finishGameReport: function(reportid) {
		var deferred = Q.defer();
		connection.query('UPDATE game_reports SET IsFinished=1 WHERE ReportID=?;', [reportid], function(err, rows, fields) {
			if (!err) {
				deferred.resolve(true);
			} else {
				deferred.resolve(false);
			}
		});
		
		return deferred.promise;
	},
	
	//Util Functions
	////////////////////////////////////
	getGlobalEntitlements: function(userID) {
		var deferred = Q.defer();
		connection.query('SELECT * FROM global_entitlements', [userID], function(err, rows, fields) {
			if (!err && rows.length != 0) {
				deferred.resolve(rows);
			} else {
				deferred.resolve(false);
			}
		});
		
		return deferred.promise;
	},
	
	//User Functions
	////////////////////////////////////
	newBlazeUser: function(blazeUserID) {
		var deferred = Q.defer();
		connection.query('INSERT INTO users (`BlazeUserID`) VALUES (?);', [blazeUserID], function(err, rows, fields) {
			if (!err) {
				deferred.resolve(module.exports.getUserByBlazeID(blazeUserID));
			} else {
				deferred.resolve(false);
			}
		});
		
		return deferred.promise;
	},

	getUserByBlazeID: function(blazeUserID) {
		var deferred = Q.defer();
		connection.query('SELECT * FROM users WHERE BlazeUserID=? LIMIT 1', [blazeUserID], function(err, rows, fields) {
			if (!err && rows.length == 1) {
				deferred.resolve(rows[0]);
			} else {
				if(rows.length == 0) {
					deferred.resolve(module.exports.newBlazeUser(blazeUserID));
				} else {
					deferred.resolve(false);
				}
			}
		});
		
		return deferred.promise;
	},
	
	getUserById: function(userID) {
		var deferred = Q.defer();
		connection.query('SELECT * FROM users WHERE UserID=? LIMIT 1', [userID], function(err, rows, fields) {
			if (!err && rows.length == 1) {
				deferred.resolve(rows[0]);
			} else {
				deferred.resolve(false);
			}
		});
		
		return deferred.promise;
	},
	
	updateUserAttribute: function(userID, attributeValue) {
		var deferred = Q.defer();
		connection.query('UPDATE users SET UserAttribute=? WHERE UserID=?;', [attributeValue, userID], function(err, rows, fields) {
			if (!err) {
				deferred.resolve(true);
			} else {
				deferred.resolve(false);
			}
		});
		
		return deferred.promise;
	},
}