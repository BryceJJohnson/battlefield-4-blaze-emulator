var tls             = require('tls');
var BlazeEncoder 	= require('../Protocol/Encoder');
var BlazeDecoder 	= require('../Protocol/Decoder');
var Enums 			= require('../Protocol/Enums');
var util            = require('util');
var Q		        = require("q");

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

var options = {
    rejectUnauthorized: false
}

function BlazeAuth(authCode) {
	var deferred 	= Q.defer();

	var socket = tls.connect(10071, '511404-gosprapp1197.ea.com', options, function() {
        var responsePacket = new BlazeEncoder(0x0023, 0x000A, 0x00, Enums.MessageType.MESSAGE, 0);
        responsePacket.writeString("AUTH", authCode);
        responsePacket.writeBlob("EXTB", "");
        responsePacket.writeInteger("EXTI", 0);
        responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
    });

    var dataBuffer = "";

    socket.onPacket = function (blazePacket) {
		//console.log(util.inspect(blazePacket, false, null));
        if(blazePacket.packet_component == 0x7802 && blazePacket.packet_command == 0x0008) {
			deferred.resolve(blazePacket);
        }
    };

	socket.parseData = function(data) {
	    var allData = false;
		if (dataBuffer.length != 0 && dataBuffer != data) {
	        dataBuffer = (dataBuffer+data)
			data = dataBuffer
        }
				
		var packetLength = (parseInt(data.substring(0, 4), 16)*2)+24;
		if(parseInt(data.substring(16, 20), 16) == 0x10) {
			packetLength += 4;
		}
			
		if (data.length >= packetLength) {
			if (dataBuffer.length != 0) {
				dataBuffer = "";
			}

			allData = true;
				
			dataBuffer = data.substring(packetLength)
			data = data.substring(0, packetLength)
				
			if(dataBuffer.length > 0)
				socket.parseData(dataBuffer)
		} else {
	        dataBuffer = data;
		}
		
		if(data.length == 0 || allData == false)
			return;

	    var blazePacket = new BlazeDecoder(data);

        socket.onPacket(blazePacket);
    };

    socket.addListener('data', function(data) {
	    var data = data.toString('hex');
        socket.parseData(data);
    });

    socket.addListener('error', function(error) {
		deferred.resolve(false);
    });

	// Set Auth Timeout from offical blaze server at 40 Seconds
	setTimeout(function() {
		console.log("BlazeAuth Timeout 40s")
        deferred.resolve(false);
    }, 40000);

	return deferred.promise;
}
//BlazeAuth("QUOQANw7j8KPuaErg5e9kbBZb7xN4MhqlSc57ea4AQ");
module.exports = BlazeAuth;