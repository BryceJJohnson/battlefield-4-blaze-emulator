var onlineServers = 0;

module.exports = function (options, port, ssl) {
	var tls;
	if(ssl)
		tls = require('tls');
	else
		tls = require('net');
	
	var Enums 			= require('../Protocol/Enums');
	var BlazeDecoder 	= require('../Protocol/Decoder');
	var sessionManager 	= require('./sessionManager');
	 
	var components = []
	var normalizedPath = require("path").join(__dirname, "../Components");
	require("fs").readdirSync(normalizedPath).forEach(function(file) {
		components.push(require("../Components/" + file));
	});

	console.log("TLS server started.");
	onlineServers++;

	var server = tls.createServer(options, function (socket) {
		
		console.log("TLS connection established");
		socket.id = socket.remoteAddress + ':' + socket.remotePort;
		
		socket.dataBuffer = "";
		
		server.socketList.push(socket);
		
		socket.parseData = function(data) {
			allData = false;
			if (socket.dataBuffer.length != 0 && socket.dataBuffer != data) {
				socket.dataBuffer += data;
				data = socket.dataBuffer;
			}
			var packet_component 	= parseInt(data.substring(4, 8), 16);
			var packet_command 		= parseInt(data.substring(8, 12), 16);
			var packetLength = (parseInt(data.substring(0, 4), 16)*2)+24;
			if(parseInt(data.substring(16, 20), 16) == 0x10) {
				packetLength = parseInt((data.substring(24, 28)+data.substring(0, 4)), 16)*2;
				packetLength += 28;
			}
			
			if (data.length >= packetLength) {
				if (socket.dataBuffer.length != 0) {
					socket.dataBuffer = "";
				}
				allData = true;
				
				socket.dataBuffer = data.substring(packetLength);
				data = data.substring(0, packetLength);
				
				if(socket.dataBuffer.length > 0)
					socket.parseData(socket.dataBuffer)
			} else {
				socket.dataBuffer = data;
			}
			
			if(data.length == 0 || allData == false)
				return;
			
			var blazePacket = new BlazeDecoder(data)
			for (i = 0; i < components.length; i++) {
				if(components[i].getId() == blazePacket.packet_component) {
					components[i].handlePacket(socket, blazePacket);
					return true;
				}
			}
			
			console.log("UNKNOWN [" + blazePacket.packet_component.toString(16) + "]");
		}
		
		socket.on("end", function (err) {
			try {
				if(socket.Blaze_User.getClientType() != Enums.ClientType.CLIENT_TYPE_DEDICATED_SERVER) {
					socket.Blaze_User.saveInventory();
				}
				sessionManager.removeUserById(socket.Blaze_User.getUserID());
			} catch (error) {};
			
			var index = server.socketList.indexOf(socket);
			server.socketList.splice(index, 1);
		});
		
		socket.on("error", function (err) {
		});
		
		socket.on("close", function (err) {
		});
		
		socket.on("timeout", function (err) {
		});
		
		socket.on("drain", function (err) {
		});
		
		socket.addListener("data", function (data) {
			data 	= data.toString('hex');
			socket.parseData(data);
		});
	 
		socket.pipe(socket);
	});
	
	server.socketList = [];
	
	server.listen(port, "0.0.0.0");
	
	
	var gracefulShutdown = function() {
	
		console.log("Received kill signal, shutting down gracefully. Timeout=30s Per Server");
		
		for(var i=0; i<server.socketList.length; i++) {
			server.socketList[i].end();
		}
		
		server.close(function() {
			onlineServers--;
			console.log("Shutdown a server. " + onlineServers + " remaining.");
			if(onlineServers <= 0) {
				console.log("All servers shutdown, exiting process...");
				process.exit();
			}
		 });
	  
		// if after 
		setTimeout(function() {
			console.error("Could not close servers in time, forcefully shutting down.");
			process.exit();
		}, 30*1000);
	}

	// listen for TERM signal .e.g. kill 
	process.on('SIGTERM', gracefulShutdown);

	// listen for INT signal e.g. Ctrl-C
	process.on('SIGINT', gracefulShutdown);
}