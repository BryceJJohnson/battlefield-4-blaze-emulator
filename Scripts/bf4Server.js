// bf4Server
// Handles the servers data and functions relating to the servers operation

var databaseManager 		= require('./databaseManager.js');
var sessionManager 			= require('./sessionManager.js');
var Enums 					= require('../Protocol/Enums');
var uuid 					= require('node-uuid');

var NotifyPlayerRemoved				= require('../Commands/GameManager/Async/NotifyPlayerRemoved');
var NotifyGameSetup 				= require('../Commands/GameManager/Async/NotifyGameSetup');
var NotifyGameModRegisterChanged	= require('../Commands/GameManager/Async/NotifyGameModRegisterChanged');
var NotifyGameStateChange 			= require('../Commands/GameManager/Async/NotifyGameStateChange');
var NotifyGameAttribChange 			= require('../Commands/GameManager/Async/NotifyGameAttribChange');
var NotifyGameCapacityChange 		= require('../Commands/GameManager/Async/NotifyGameCapacityChange');
var NotifyPlatformHostInitialized 	= require('../Commands/GameManager/Async/NotifyPlatformHostInitialized');
var NotifyGameNameChanaged 			= require('../Commands/GameManager/Async/NotifyGameNameChanaged');
var NotifyGameReportingIdChange 	= require('../Commands/GameManager/Async/NotifyGameReportingIdChange');

var UserRemoved						= require('../Commands/UserSessions/UserRemoved');

function BF4Server(socket, serverData, hostUserID) {
	
	this.commanderMesh 		= new Array(2);
	this.spectatorMesh 		= new Array(4);
	this.playerMesh 		= new Array(64);
	this.queueMesh			= new Array(20);
	
	this.maxSoliders		= 0;
	this.maxSpectators		= 0;
	this.maxCommanders		= 0;
	
	this.socketStream 		= socket;
	this.gameID 			= sessionManager.requestGameID();
	this.gameState			= Enums.GameState.NEW_STATE;
	
	this.hostPlayerID		= hostUserID;
	
	this.attributeList 		= serverData["ATTR"][1];
	this.pingSite 			= serverData["GCTR"][1];
	this.gameEntryType 		= serverData["GENT"][1];
	this.gameManagerID 		= serverData["GMRG"][1];
	this.gameName 			= serverData["GNAM"][1];
	this.gameSet 			= serverData["GSET"][1];
	this.gameType 			= serverData["GTYP"][1];
	this.gameURL 			= serverData["GURL"][1];
	this.hostNet 			= serverData["HNET"][1];
	this.gameIgno 			= serverData["IGNO"][1];
	this.gameNres 			= serverData["NRES"][1];
	this.gameNtop 			= serverData["NTOP"][1];
	this.persistentID		= serverData["PGID"][1];
	this.persistentIDSecret	= serverData["PGSC"][1];
	this.maxPlayers			= serverData["PMAX"][1];
	this.playerCapacity		= serverData["PCAP"][1];
	this.presenceMode		= serverData["PRES"][1];
	this.queueMax			= serverData["QCAP"][1];
	this.rgid				= serverData["RGID"][1];
	this.roleInfo			= serverData["RNFO"][1];
	this.hostSlot			= serverData["SLOT"][1];
	this.teamIDs			= serverData["TIDS"][1];
	this.tidx				= serverData["TIDX"][1];
	this.voipSetting		= serverData["VOIP"][1];
	this.versionString		= serverData["VSTR"][1];	
	this.meshID				= "3f52c3ef-d1a7-42e9-bec4-31f1bfba93a6";
	
	this.maxSoliders		= this.playerCapacity[0];
	this.maxSpectators		= this.playerCapacity[2];
	this.maxCommanders		= 2;

	
	if(this.persistentID == "" || this.persistentIDSecret == "") {
		this.persistentID 			= uuid.v4();
		this.persistentIDSecret 	= "34c99b7735b790570ae131f7acbe3fa88cdef54ff330f45560c44d919bc3231c532482b4a65c9c38295a89ccacc8cf87f6d5bc8fd5add3c2";
	}
	
	this.gameReportingID 	= databaseManager.setupNewGameReport(this.gameID, this.persistentID, this.gameType);
}

BF4Server.prototype.setUpGame = function() {
	var blazeServer = this;
	
	this.gameReportingID.then(function(result) {
		if(result) {
			blazeServer.gameReportingID = result;
			
			sessionManager.addServerByID(blazeServer);
			blazeServer.changeState(Enums.GameState.INITIALIZING);
			NotifyGameSetup.createResponse(blazeServer.socketStream, blazeServer);
		}
	});
}

BF4Server.prototype.finishGame = function() {
	var blazeServer 		= this;
	
	databaseManager.finishGameReport(this.gameReportingID);
	
	databaseManager.setupNewGameReport(this.gameID, this.persistentID, this.gameType).then(function (result) {
		if(result) {
			blazeServer.gameReportingID = result;
			NotifyGameReportingIdChange.createResponse(blazeServer.socketStream, blazeServer);
		}
	});
}

BF4Server.prototype.finishCreating = function() {
	NotifyPlatformHostInitialized.createResponse(this.socketStream, this.gameID, this.hostPlayerID, 0);
}

BF4Server.prototype.changeState = function(stateID) {
	this.gameState = stateID;
	NotifyGameStateChange.createResponse(this.socketStream, this);
}

BF4Server.prototype.changeAttributes = function(changedValues) {
	for(key in this.attributeList) {
		for(changeKey in changedValues) {
			if(key == changeKey) {
				this.attributeList[key] = changedValues[changeKey];
			}
		}
	}
	
	NotifyGameAttribChange.createResponse(this.socketStream, this, changedValues);
}

BF4Server.prototype.changeCapacity = function(capacityInfo, newRoleInfo) {
	this.roleInfo 		= newRoleInfo;
	this.playerCapacity = capacityInfo;
	
	NotifyGameCapacityChange.createResponse(this.socketStream, this);
}

BF4Server.prototype.changeName = function(newGameName) {
	this.gameName 		= newGameName;
	NotifyGameNameChanaged.createResponse(this.socketStream, this);
}

BF4Server.prototype.changeGameModRegister = function(newRegister) {
	this.gameManagerID 		= newRegister;
	NotifyGameModRegisterChanged.createResponse(this.socketStream, this, newRegister);
}

//Connection mesh shit

BF4Server.prototype.playerJoinFromType = function(userid, type) {
	if(type == Enums.SlotType.PLAYER) {
		return this.playerJoin(userid);
	} else if(type == Enums.SlotType.SPECTATOR) {
		return this.spectatorJoin(userid);
	}
	
	return false;
}
BF4Server.prototype.removeUser = function(userid, reason) {
	var hasLeft = false;
	
	for(var i=0; i<this.playerMesh.length; i++) {
		if(this.playerMesh[i] == userid) {
			hasLeft = this.playerLeave(userid);
		}
	}
	
	for(var i=0; i<this.spectatorMesh.length; i++) {
		if(this.spectatorMesh[i] == userid) {
			hasLeft = this.spectatorLeave(userid);
		}
	}
	
	for(var i=0; i<this.commanderMesh.length; i++) {
		if(this.commanderMesh[i] == userid) {
			hasLeft = this.commanderLeave(userid);
		}
	}
	
	if(hasLeft) {
		UserRemoved.createResponse(this.socketStream, userid);
		NotifyPlayerRemoved.createResponse(this.socketStream, this.gameID, userid, reason);
	}
	
	return hasLeft;
}
BF4Server.prototype.playerJoin = function(userid) {
	if(this.getAllPlayers() < this.maxPlayers && this.getCurrentPlayers() < this.maxSoliders) {
		for(var i=0; i<this.playerMesh.length; i++) {
			if(this.playerMesh[i] === undefined || this.playerMesh[i] == 0) {
				this.playerMesh[i] = userid;
				return i+1;
			}
		}
	}
	return false;
}
BF4Server.prototype.playerLeave = function(userid) {
	for(var i=0; i<this.playerMesh.length; i++) {
		if(this.playerMesh[i] === userid) {
			this.playerMesh[i] = 0;
			return true;
		}
	}
	return false;
}
BF4Server.prototype.spectatorJoin = function(userid) {
	if(this.getAllPlayers() < this.maxPlayers && this.getCurrentSpectators() < this.maxSpectators) {
		for(var i=0; i<this.spectatorMesh.length; i++) {
			if(this.spectatorMesh[i] === undefined || this.spectatorMesh[i] == 0) {
				this.spectatorMesh[i] = userid;
				return i+1;
			}
		}
	}
	return false;
}
BF4Server.prototype.spectatorLeave = function(userid) {
	for(var i=0; i<this.spectatorMesh.length; i++) {
		if(this.spectatorMesh[i] === userid) {
			this.spectatorMesh[i] = 0;
			return true;
		}
	}
	return false;
}
BF4Server.prototype.commanderJoin = function(userid) {
	if(this.getAllPlayers() < this.maxPlayers && this.getCurrentCommanders() < this.maxCommanders) {
		for(var i=0; i<this.commanderMesh.length; i++) {
			if(this.commanderMesh[i] === undefined || this.commanderMesh[i] == 0) {
				this.commanderMesh[i] = userid;
				return i+1;
			}
		}
	}
	return false;
}
BF4Server.prototype.commanderLeave = function(userid) {
	for(var i=0; i<this.commanderMesh.length; i++) {
		if(this.commanderMesh[i] === userid) {
			this.commanderMesh[i] = 0;
			return true;
		}
	}
	return false;
}

BF4Server.prototype.getAllPlayers = function() {
	var allPlayers = 0;
	allPlayers += this.getCurrentPlayers();
	allPlayers += this.getCurrentSpectators();
	allPlayers += this.getCurrentCommanders();
	return allPlayers;
}

BF4Server.prototype.getCurrentPlayers = function() {
	var currentPlayers = 0;
	for(var i=0; i<this.playerMesh.length; i++) {
		if(this.playerMesh[i] !== undefined && this.playerMesh[i] != 0) {
			currentPlayers++;
		}
	}
	return currentPlayers;
}

BF4Server.prototype.getCurrentCommanders = function() {
	var currentPlayers = 0;
	for(var i=0; i<this.commanderMesh.length; i++) {
		if(this.commanderMesh[i] !== undefined && this.commanderMesh[i] != 0) {
			currentPlayers++;
		}
	}
	return currentPlayers;
}

BF4Server.prototype.getCurrentSpectators = function() {
	var currentPlayers = 0;
	for(var i=0; i<this.spectatorMesh.length; i++) {
		if(this.spectatorMesh[i] !== undefined && this.spectatorMesh[i] != 0) {
			currentPlayers++;
		}
	}
	return currentPlayers;
}

///////////// Getter Functions \\\\\\\\\\\\\ 
BF4Server.prototype.getSocket = function() {
	return this.socketStream;
}
BF4Server.prototype.getGameID = function() {
	return this.gameID;
}
BF4Server.prototype.getGameReportingID = function() {
	return this.gameReportingID;
}
BF4Server.prototype.getGameState = function() {
	return this.gameState;
}
BF4Server.prototype.getConnectionID = function() {
	return this.connectionID;
}
BF4Server.prototype.getHostingPlayerID = function() {
	return this.hostPlayerID;
}
BF4Server.prototype.getAttributeList = function() {
	return this.attributeList;
}
BF4Server.prototype.getPingSite = function() {
	return this.pingSite;
}
BF4Server.prototype.getGameEntryType = function() {
	return this.gameEntryType;
}
BF4Server.prototype.getGameManagerID = function() {
	return this.gameManagerID;
}
BF4Server.prototype.getGameName = function() {
	return this.gameName;
}
BF4Server.prototype.getGameSet = function() {
	return this.gameSet;
}
BF4Server.prototype.getGameType = function() {
	return this.gameType;
}
BF4Server.prototype.getGameURL = function() {
	return this.gameURL;
}
BF4Server.prototype.getHostNetworkInfo = function() {
	return this.hostNet;
}
BF4Server.prototype.getIGNO = function() {
	return this.gameIgno;
}
BF4Server.prototype.getNRES = function() {
	return this.gameNres;
}
BF4Server.prototype.getNetworkTopology = function() {
	return this.gameNtop;
}
BF4Server.prototype.getPersistentID = function() {
	return this.persistentID;
}
BF4Server.prototype.getPersistentIDSecret = function() {
	return this.persistentIDSecret;
}
BF4Server.prototype.getMaxPlayers = function() {
	return this.maxPlayers;
}
BF4Server.prototype.getPlayerCapacityInfo = function() {
	return this.playerCapacity;
}
BF4Server.prototype.getPresenceMode = function() {
	return this.presenceMode;
}
BF4Server.prototype.getMaxQueueSize = function() {
	return this.queueMax;
}
BF4Server.prototype.getRGID = function() {
	return this.rgid;
}
BF4Server.prototype.getRoleInfo = function() {
	return this.roleInfo;
}
BF4Server.prototype.getHostSlot = function() {
	return this.hostSlot;
}
BF4Server.prototype.getTeamIDs = function() {
	return this.teamIDs;
}
BF4Server.prototype.getTidx = function() {
	return this.tidx;
}
BF4Server.prototype.getVoipSetting = function() {
	return this.voipSetting;
}
BF4Server.prototype.getVersion = function() {
	return this.versionString;
}
BF4Server.prototype.getUUID = function() {
	return this.meshID;
}

module.exports = BF4Server;