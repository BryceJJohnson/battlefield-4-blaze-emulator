// sessionManager
// Manages global session variables for Blaze.

var users		= [];
var servers 	= [];

var sessionGameID = 1;

module.exports = {
	// Server funcs
	requestGameID: function() {
		return sessionGameID++;
	},
	
	// USER NORMAL FUNCTIONS
	addUser: function(user) {
		for(i = 0; i < users.length; i++) {
			if(users[i].getUserID() == user.getUserID()) {
				return false;
			}
		}
		users.push(user);	
		return true;
	},
	getUserById: function(UserID) {
		for(i = 0; i < users.length; i++) {
			if(users[i].getUserID() == UserID) {
				return users[i];
			}
		}
		return false;
	},
	removeUserById: function(UserID) {
		for(i = 0; i < users.length; i++) {
			if(users[i].getUserID() == UserID) {
				users.splice(i, 1);
			}
		}
	},
	
	// SERVER FUNCTIONS
	addServerByID: function(server) {
		for(i = 0; i < servers.length; i++) {
			if(servers[i].getGameID() == server.getGameID()) {
				return false;
			}
		}
		servers.push(server);
		return true;
	},
	getServerById: function(gameID) {
		for(i = 0; i < servers.length; i++) {
			if(servers[i].getGameID() == gameID) {
				return servers[i];
			}
		}
		return false;
	},
	getServerByReportId: function(reportID) {
		for(i = 0; i < servers.length; i++) {
			if(servers[i].getGameReportingID() == reportID) {
				return servers[i];
			}
		}
		return false;
	},
	removeServerById: function(gameID) {
		for(i = 0; i < servers.length; i++) {
			if(servers[i].getGameID() == gameID) {
				servers.splice(i, 1);
			}
		}
	}
}

