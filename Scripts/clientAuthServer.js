var https			= require('https');
var fs 				= require('fs');
var dataBase 		= require('../Scripts/databaseManager');
var sessionManager 	= require('../Scripts/sessionManager');


const options = {
	key: fs.readFileSync('BlazeData/ssl/key.pem'),
	cert: fs.readFileSync('BlazeData/ssl/cert.pem')
};

https.createServer(options, function (request, response) {
	//console.log(request.url);
	//console.log(request.headers);
	//console.log(request.method);
	
	var body = [];
	request.on('data', function(chunk) {
		//console.log("BODY: " + chunk);
	});
	
	if(request.method === "GET") {
		
		if(request.url.indexOf("/api/bf4/pc/battledash/1/widgetdata/") > -1) {
			fs.readFile('BlazeData/bf4/battledash.json', function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end();
				}
				else {
					//response.setHeader("Content-Length", content.toString().length);
					response.writeHead(200, { 'Content-Type': 'text/html' });
					response.end(content, 'utf-8');
				}
			});
			return true;
		}
		
		if(request.url.indexOf("/bf4/battledash") > -1) {
			fs.readFile('BlazeData/bf4/battledash.html', function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.setHeader("Content-Length", content.toString().length);
					response.writeHead(200, { 'Content-Type': 'text/html' });
					response.end(content, 'utf-8');
				}
			});
			return true;
		}
		
		var file = undefined;
		if (request.url === "/Battlefield/4/config/pc/game.xml") {
			file = "game.xml";
		}
		if (request.url === "/Battlefield/4/config/pc/server.xml") {
			file = "server.xml";
		}
		
		if(file !== undefined) {
			fs.readFile('BlazeData/bf4/'+file, function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.writeHead(200, { 'Content-Type': 'text/xml' });
					response.end(content, 'utf-8');
				}
			});
			return true;
		}
		
		response.writeHead(404);
		response.end("File not found");
	} else {
		if (request.url === "/api/pc/token/2/identity") {
			response.setHeader("Content-Length", 42);
			response.setHeader("Cache-Control", "max-age=0, no-cache, no-store");
			response.setHeader("Pragma", "no-cache");
			response.setHeader("X-Node", "127.0.0.1");
			response.writeHead(200, { 'Content-Type': 'text/html' });
			response.write('"2a1327402a8547c5819d8184af641e8da0a97d06"');
			response.end();
		} else {
			response.writeHead(404);
			response.end();
		}
	}
}).listen(443);