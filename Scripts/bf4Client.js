// blazeClient
// Handles the clients data and functions

var databaseManager 		= require('./databaseManager.js');
var sessionManager 			= require('./sessionManager');
var Enums 					= require('../Protocol/Enums');
var BlazeUtils 				= require('../Protocol/BlazeUtils');

var fs 						= require('fs-extra')
var path  					= require("path");
var uuid 					= require('node-uuid');

var NotifyPlayerRemoved				= require('../Commands/GameManager/Async/NotifyPlayerRemoved');
var NotifyPlayerJoinCompleted		= require('../Commands/GameManager/Async/NotifyPlayerJoinCompleted');
var NotifyGamePlayerStateChange 	= require('../Commands/GameManager/Async/NotifyGamePlayerStateChange');
var UserSessionExtendedDataUpdate 	= require('../Commands/UserSessions/UserSessionExtendedDataUpdate');
var UserRemoved 					= require('../Commands/UserSessions/UserRemoved');

var util            = require('util');


function BF4Client(socket, userData, blazeDetails, blazeUser) {
	this.socketStream 	= socket;
	
	//Player Session Data
	if(blazeDetails.Type == Enums.ClientType.CLIENT_TYPE_DEDICATED_SERVER) {
		this.blazeUserID 	= userData.BlazeUserID;
		this.displayName 	= userData.DisplayName;
	} else {
		this.blazeUserID 	= blazeUser.packet_decoded["BUID"][1];
		this.displayName 	= blazeUser.packet_decoded["DSNM"][1];
	}
	
	
	this.userID 		= userData.UserID;
	this.eMail			= "na@gmail.com";
	this.sessionKey		= uuid.v4();
	this.platform		= 4;
	this.lastLogin		= userData.LastLogin;
	this.lastAuth		= userData.LastLogin;
	this.accountStatus 	= userData.Status;
	this.userSettings 	= userData.UserSettings;
	this.userAttribute 	= userData.UserAttribute;
	
	//Player Inventory
	this.playerItems		= [];
	this.playerPacks		= [];
	this.playerConsumables	= [];
	
	this.playerUserSettings = [];
	
	//Player Network
	this.networkQuality = [];
	this.pingSiteList	= [];
	this.networkInfo 	= [];
	this.serverInfo		= [];
	this.joinData		= [];
	this.hostingServers = [];
	this.currentGameId	= 0;
	this.currentSlotId 	= 0;

	this.blazeObjects	= [];

	this.playerState 	= Enums.PlayerState.ACTIVE_CONNECTING;
	
	//Blaze Specific Details
	this.gameName 		= blazeDetails.Game;
	this.clientType 	= blazeDetails.Type;
	this.clientName 	= blazeDetails.ClientName;
	this.clientVersion 	= blazeDetails.ClientVersion;
	this.blazeVerison 	= blazeDetails.BlazeVerison;
	this.environment 	= blazeDetails.Environment;
	this.locale 		= blazeDetails.Locale;
	
	//Only clients load their shit
	if(this.clientType == Enums.ClientType.CLIENT_TYPE_GAMEPLAY_USER) {
		var statDir 			= path.join(__dirname, '../BlazeData/user_stats/'+this.userID+'/');
		this.settingsFile 		= path.join(__dirname, '../BlazeData/user_stats/'+this.userID+'/'+BlazeUtils.getGameNameFromType(this.clientName)+'_usersettings.txt');
		this.itemsFile 			= path.join(__dirname, '../BlazeData/user_stats/'+this.userID+'/'+BlazeUtils.getGameNameFromType(this.clientName)+'_items.txt');
		this.packsFile 			= path.join(__dirname, '../BlazeData/user_stats/'+this.userID+'/'+BlazeUtils.getGameNameFromType(this.clientName)+'_packs.txt');
		this.consumablesFile 	= path.join(__dirname, '../BlazeData/user_stats/'+this.userID+'/'+BlazeUtils.getGameNameFromType(this.clientName)+'_consumables.txt');
		
		var client = this;
		BlazeUtils.createDirIfNotExists(statDir, function(error) {
			if(!error) {
				client.loadInventory();
			} else {
				//TODO ERROR IF NO FOLDER FOR STATS
			}
		});
	}
}

BF4Client.prototype.loadInventory = function() {
	BlazeUtils.createEmptyFileIfNotExistJson(this.settingsFile);
	BlazeUtils.createEmptyFileIfNotExistJson(this.itemsFile);
	BlazeUtils.createEmptyFileIfNotExistJson(this.packsFile);
	BlazeUtils.createEmptyFileIfNotExistJson(this.consumablesFile);
		
	var client = this;
	fs.readJson(this.settingsFile, function(error, data) {
		if(!error) {
			client.playerUserSettings = data;
		}
	});
		
	fs.readJson(this.itemsFile, function(error, data) {
		if(!error) {
			client.playerItems = data;
		}
	});

	fs.readJson(this.packsFile, function(error, data) {
		if(!error) {
			client.playerPacks = data;
		}
	});
		
	fs.readJson(this.consumablesFile, function(error, data) {
		if(!error) {
			client.playerConsumables = data;
		}
	});
}

//Inventory!
BF4Client.prototype.saveInventory = function() {
	fs.writeJson(this.settingsFile, this.playerUserSettings);
	fs.writeJson(this.itemsFile, this.playerItems);
	fs.writeJson(this.packsFile, this.playerPacks);
	fs.writeJson(this.consumablesFile, this.playerConsumables);
}

BF4Client.prototype.givePack = function(packName, awardType) {
	if(!this.hasItem(packName)) {
		var pack = [];
		pack.push(packName);
		pack.push(awardType);
		pack.push([]);
		
		this.playerPacks.push(pack);
		this.giveItem(packName);
		this.saveInventory(); // -- Saves items, packs, consumeables and userSettings
	}
}
BF4Client.prototype.hasItem = function(itemName) {
	for(var i = 0; i < this.playerItems.length; i++) {
		if(this.playerItems[i] == itemName)
			return true;
	}
	return false;
}
BF4Client.prototype.giveItem = function(itemName) {
	if(!this.hasItem(itemName)) {
		this.playerItems.push(itemName);
		return true;
	}
	return false;
}
BF4Client.prototype.giveConsumeable = function(consumeableName) {
}

BF4Client.prototype.setPackItems = function(packid, itemList) {
	this.playerPacks[packid][2] = itemList;
	for(var i=0; i<itemList.length; i++) {
		this.giveItem(itemList[i]);
	}
	this.saveInventory();
}
BF4Client.prototype.getPacks = function() {
	return this.playerPacks;
}
BF4Client.prototype.getItems = function() {
	return this.playerItems;
}
BF4Client.prototype.getConsumeables = function() {
	return this.playerConsumables;
}


//User Functions
BF4Client.prototype.setAttribute = function(attributeValue) {
	this.userAttribute = attributeValue;
	databaseManager.updateUserAttribute(this.userID, attributeValue);
	UserSessionExtendedDataUpdate.createResponse(this.socketStream, this, true);
}
BF4Client.prototype.addBlazeObject = function(blazeObject) {
	//[Component ID, ??, id]
	this.blazeObjects.push(blazeObject);
}
BF4Client.prototype.getBlazeObjects = function() {
	//[Component ID, ??, id]
	return this.blazeObjects;
}

BF4Client.prototype.updateUserSettings = function(dataKey, data) {
	for(var i = 0; i < this.playerUserSettings.length; i++) {
		if(this.playerUserSettings[i][0] == dataKey) {
			this.playerUserSettings[i][1] = data;
			return true;
		}
	}
	
	var newSetting = [];
	newSetting.push(dataKey);
	newSetting.push(data);
	
	this.playerUserSettings.push(newSetting);
}

BF4Client.prototype.updateNetworkInfo = function(newNetworkInfo) {
	if(newNetworkInfo["ADDR"] !== undefined)
		this.networkInfo = newNetworkInfo["ADDR"][1];
	if(newNetworkInfo["NQOS"] !== undefined)
		this.networkQuality = newNetworkInfo["NQOS"][1];
	if(newNetworkInfo["NLMP"] !== undefined) {
		var num = 0;
		for(key in newNetworkInfo["NLMP"][1]) {
			this.pingSiteList[num] = (newNetworkInfo["NLMP"][1][key]);
			num++;
		}
	}
	UserSessionExtendedDataUpdate.createResponse(this.socketStream, this, true);
}

BF4Client.prototype.changePlayerState = function(stateInfo) {
	if(this.joinData == [] || this.joinData === undefined)
		return;
	
	var server = sessionManager.getServerById(stateInfo["GID "][1]);
	this.currentGameId = stateInfo["GID "][1];
	
	if(server) {
		if(stateInfo["STAT"][1] == Enums.PlayerJoiningState.ACTIVE_CONNECTING) {
			this.playerState = Enums.PlayerJoiningState.ACTIVE_CONNECTED;
		} else {
			this.playerState = stateInfo["STAT"][1];
		}
		
		if(this.playerState == Enums.PlayerJoiningState.ACTIVE_CONNECTED) {
			NotifyGamePlayerStateChange.createResponse(this.socketStream, server, this);
			NotifyGamePlayerStateChange.createResponse(server.getSocket(), server, this);
			
			NotifyPlayerJoinCompleted.createResponse(this.socketStream, server, this);
			NotifyPlayerJoinCompleted.createResponse(server.getSocket(), server, this);
		}
		
	}
}

BF4Client.prototype.setJoinData = function(newJoinData) {
	this.joinData = newJoinData;
}
BF4Client.prototype.addHostingServer = function(gameID) {
	this.hostingServers.push(gameID);
}
BF4Client.prototype.setCurrentSlotID = function(slot) {
	this.currentSlotId = slot;
}

///////////// Getter Functions \\\\\\\\\\\\\

BF4Client.prototype.getCurrentSlotID = function() {
	return this.currentSlotId;
}
BF4Client.prototype.getCurrentGameID = function() {
	return this.currentGameId;
}
BF4Client.prototype.getAttribute = function() {
	return this.userAttribute;
}
BF4Client.prototype.getUserSettings = function() {
	return this.playerUserSettings;
}
BF4Client.prototype.getUserSetting = function(dataKey) {
	for(var i = 0; i < this.playerUserSettings.length; i++) {
		if(this.playerUserSettings[i][0] == dataKey) {
			return this.playerUserSettings[i][1];
		}
	}
	return false;
}
BF4Client.prototype.getGameName = function() {
	return this.gameName;
}
BF4Client.prototype.getClientType = function() {
	return this.clientType;
}
BF4Client.prototype.getClientName = function() {
	return this.clientName;
}
BF4Client.prototype.getClientVersion = function() {
	return this.clientVersion;
}
BF4Client.prototype.getBlazeVerison = function() {
	return this.blazeVerison;
}
BF4Client.prototype.getEnvironment = function() {
	return this.environment;
}
BF4Client.prototype.getLocale = function() {
	return this.locale;
}
BF4Client.prototype.getPingSiteList = function() {
	return this.pingSiteList;
}
BF4Client.prototype.getNetworkQuality = function() {
	return this.networkQuality;
}
BF4Client.prototype.getSocket = function() {
	return this.socketStream;
}
BF4Client.prototype.getBlazeUserID = function() {
	return this.blazeUserID;
}
BF4Client.prototype.getUserID = function() {
	return this.userID;
}
BF4Client.prototype.getDisplayName = function() {
	return this.displayName;
}
BF4Client.prototype.getEmail = function() {
	return this.eMail;
}
BF4Client.prototype.getSessionKey = function() {
	return this.sessionKey;
}
BF4Client.prototype.getPlatform = function() {
	return this.platform;
}
BF4Client.prototype.getLastLogin = function() {
	return this.lastLogin;
}
BF4Client.prototype.getLastAuth = function() {
	return this.lastAuth;
}
BF4Client.prototype.isFirstLogin = function() {
	return ((this.lastLogin > 0) ? 1 : 0);
}
BF4Client.prototype.getAccountStatus = function() {
	return this.accountStatus;
}
BF4Client.prototype.getUserSettings = function() {
	return this.userSettings;
}
BF4Client.prototype.getNetworkInfo = function() {
	return this.networkInfo;
}
BF4Client.prototype.getJoinData = function() {
	return this.joinData;
}
BF4Client.prototype.getState = function() {
	return this.playerState;
}

module.exports = BF4Client;