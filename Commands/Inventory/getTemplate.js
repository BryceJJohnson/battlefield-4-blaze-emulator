// Inventory Component
// getTemplate Command
// Gets the item list for a game
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var fs  			= require("fs");
var dataBase 		= require('../../Scripts/databaseManager');

var itemListFile 	= require("path").join(__dirname, "../../BlazeData/bf4/items.txt");
var itemList = [];
fs.readFileSync(itemListFile).toString().split('\n').forEach(function (line) { 
	itemList.push(line.replace(/(\r\n|\n|\r)/gm,""))
});

var componentId = 0x0803;
var id 			= 0x0006;
var name 		= "getTemplate";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	getItemList: function() {
		return itemList;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.writeList("ILST", itemList, Enums.TagType.TYPE_STRING);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};