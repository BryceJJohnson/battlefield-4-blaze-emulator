// Inventory Component
// drainConsumable Command
// Drains a players consumeable
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var fs  			= require("fs");

var itemListFile 	= require("path").join(__dirname, "../../BlazeData/bf4/items.txt");
var itemList = [];
fs.readFileSync(itemListFile).toString().split('\n').forEach(function (line) { 
	itemList.push(line.replace(/(\r\n|\n|\r)/gm,""))
});

var componentId = 0x0803;
var id 			= 0x0005;
var name 		= "drainConsumable";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(blazePacket);
		
		var contentFilter 	= blazePacket.packet_decoded["CFLG"][1];
		var returnFilter	= blazePacket.packet_decoded["RTYP"][1];
		
		var userID 			= blazePacket.packet_decoded["UID "][1];
		var itemKey			= blazePacket.packet_decoded["IKEY"][1];
		var drainAmount		= blazePacket.packet_decoded["DSEC"][1];
		if(userID == 0 && socket.Blaze_User.getClientType() == Enums.ClientType.CLIENT_TYPE_GAMEPLAY_USER) {
			userID = socket.Blaze_User.getUserID();
		}


		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.writeInteger("LEFT", 0);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};