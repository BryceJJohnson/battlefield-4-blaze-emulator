// Inventory Component
// getIems Command
// Retrieves a players items
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var fs  			= require("fs");

var itemListFile 	= require("path").join(__dirname, "../../BlazeData/bf4/items.txt");
var itemList = [];
fs.readFileSync(itemListFile).toString().split('\n').forEach(function (line) { 
	itemList.push(line.replace(/(\r\n|\n|\r)/gm,""))
});

var componentId = 0x0803;
var id 			= 0x0001;
var name 		= "getItems";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(blazePacket);
		
		var contentFilter 	= blazePacket.packet_decoded["CFLG"][1];
		var returnFilter	= blazePacket.packet_decoded["RTYP"][1];
		
		var userID 			= blazePacket.packet_decoded["UID "][1];
		if(userID == 0 && socket.Blaze_User.getClientType() == Enums.ClientType.CLIENT_TYPE_GAMEPLAY_USER) {
			userID = socket.Blaze_User.getUserID();
		}


		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		var invtData = [];
		
		var blstData = [];
		var clstData = [];
		var ilstData = [];
		var ulstData = [];
		
		if(userID != 0) {
			
			var client = sessionManager.getUserById(userID);
			
			// Bool list for if a user has an item or not
			// If Item List or ALL content filter
			if(contentFilter == Enums.InventoryType.ITEMS || contentFilter == Enums.InventoryType.ALL) {
				if(returnFilter == Enums.InventoryReturnType.BOOL_LIST) {
					for(var i=0; i < itemList.length; i++) {
						blstData.push(((client.hasItem(itemList[i])) ? 1 : 0));
					}
				}
				
				if(returnFilter == Enums.InventoryReturnType.ITEM_LIST) {
					ulstData = client.getItems();
				}
				
			}
		}
		//Bool list of player Items
		invtData["BLST"] = [Enums.TagType.TYPE_LIST, blstData, Enums.TagType.TYPE_INTEGER];
		
		//List of player's consumeables
		invtData["CLST"] = [Enums.TagType.TYPE_LIST, clstData, Enums.TagType.TYPE_STRUCT];
		
		//Unknown Type
		//invtData["ILST"] = [Enums.TagType.TYPE_LIST, ilstData, Enums.TagType.TYPE_STRUCT];
		
		//Player Items in string list
		invtData["ULST"] = [Enums.TagType.TYPE_LIST, ulstData, Enums.TagType.TYPE_STRING];
		
		responsePacket.writeStruct("INVT", invtData);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};