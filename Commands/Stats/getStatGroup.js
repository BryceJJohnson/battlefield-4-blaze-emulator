// Stats Component
// getStatGroup Command
// Gets the stats in their default groups
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var fs  			= require("fs");
var path			= require("path");

/*
var statList 						= [];
statList["player_awards"] 			= [];
statList["player_awardsDogTags"] 	= [];
statList["player_awardsXP"] 		= [];
statList["player_core"] 			= [];
statList["player_statcategory"] 	= [];
statList["player_unknown"] 			= [];
statList["player_weapons1"] 		= [];
statList["player_weaponsxp"] 		= [];
statList["spplayer_singleplayer"] 	= [];


for(category in statList) {
	var tmpFile = path.join(__dirname, "../../BlazeData/bf4/statGroups/"+category+".txt");
	var statValues = [];
	fs.readFileSync(tmpFile).toString().split('\n').forEach(function (line) {
		if(line != '')
			statValues.push(line.replace(/(\r\n|\n|\r)/gm,""))
	});
	
	statList[category] = statValues;
}
*/
var componentId = 0x0007;
var id 			= 0x0004;
var name 		= "getStatGroup";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(blazePacket.packet_decoded);
		
		var statSpace = blazePacket.packet_decoded["NAME"][1];
		
		//console.log(statSpace);
		
		if(statSpace == "player_mpdefault")
			statSpace = "player_WebPlayerStats";
		
		var statList = [];
		
		var categoryDir = path.join(__dirname, "../../BlazeData/default_stats/"+statSpace+"/");
		fs.readdirSync(categoryDir).forEach(function(file) {
			file = file.substring(0,file.length-4);
			statList[file] = [];
		});
		
		for(category in statList) {
			var tmpFile = path.join(__dirname, "../../BlazeData/default_stats/"+statSpace+"/"+category+".txt");
			var statValues = [];
			fs.readFileSync(tmpFile).toString().split('\n').forEach(function (line) {
				if(line != '')
					statValues.push(line.replace(/(\r\n|\n|\r)/gm,""))
			});
			
			statList[category] = statValues;
		}
		
		
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.LARGE_RESPOSNE, blazePacket.packet_id);
		
		responsePacket.writeString("CNAM", "player_awards");
		responsePacket.writeString("DESC", statSpace);
		responsePacket.writeString("NAME", statSpace);
		
		
		var statData = [];
		for(category in statList) {
			for(statValue in statList[category]) {
				var statArray = [];
				statArray["CATG"] = [Enums.TagType.TYPE_STRING, category];
				statArray["DFLT"] = [Enums.TagType.TYPE_STRING, "0.00"];
				statArray["FRMT"] = [Enums.TagType.TYPE_STRING, "%.2f"];
				statArray["NAME"] = [Enums.TagType.TYPE_STRING, statList[category][statValue]];
				statArray["TYPE"] = [Enums.TagType.TYPE_INTEGER, 1];
				statData.push(statArray);
			}
		}
		responsePacket.writeList("STAT", statData, Enums.TagType.TYPE_STRUCT);
		
		responsePacket.buildPacket();
		//console.log(responsePacket.packet_complete);
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};