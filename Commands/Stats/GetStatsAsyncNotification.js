// Stats Component
// GetStatsAsyncNotification Command
// Sent when a users stats have compiled
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var fs  			= require("fs-extra");
var path  			= require("path");

var componentId = 0x0007;
var id 			= 0x0032;
var name 		= "GetStatsAsyncNotification";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, userList, vid) {
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, 0x0);
		
		var statsSpace = "player_WebPlayerStats";
		
		responsePacket.writeString("GRNM", "player_WebPlayerStats");
		responsePacket.writeString("KEY ", "No_Scope_Defined");
		responsePacket.writeInteger("LAST", 1);
		
		var stsData = [];
		var userData = [];
		for(user in userList) {
			user = userList[user];
			var tmpData = [];
			tmpData["EID "] = [Enums.TagType.TYPE_INTEGER, user];
			
			var statData = [];
			
			var statFile 	= path.join(__dirname, "../../BlazeData/user_stats/"+user+"/"+statsSpace+".txt");
			var statFileExists 	= fs.existsSync(statFile);
			if(!statFileExists)
				fs.copySync(path.join(__dirname, "../../BlazeData/default_stats/"+statsSpace+".txt"), statFile)
			
			fs.readFileSync(statFile).toString().split('\n').forEach(function (line) {
				if(line != '') {
					statData.push(line.replace(/(\r\n|\n|\r)/gm,"").split("=")[1])
				}
			});
			tmpData["STAT"] = [Enums.TagType.TYPE_LIST, statData, Enums.TagType.TYPE_STRING];
			userData.push(tmpData);
		}
		
		stsData["STAT"] = [Enums.TagType.TYPE_LIST, userData, Enums.TagType.TYPE_STRUCT];
		responsePacket.writeStruct("STS ", stsData);
		responsePacket.writeInteger("VID ", vid);
		
		responsePacket.buildPacket();
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};