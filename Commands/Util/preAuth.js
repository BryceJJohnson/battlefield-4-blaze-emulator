// Util Component
// preAuth Command
// Gives the socket the required blaze information.
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x0009;
var id 			= 0x0007;
var name 		= "preAuth";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		socket.BF_Data = {};
		socket.BF_Data.Game 			= blazePacket.packet_decoded["CDAT"][1]["SVCN"][1];
		socket.BF_Data.Type 			= blazePacket.packet_decoded["CDAT"][1]["TYPE"][1];
		socket.BF_Data.ClientName 		= blazePacket.packet_decoded["CINF"][1]["CLNT"][1];
		socket.BF_Data.ClientVersion 	= blazePacket.packet_decoded["CINF"][1]["CVER"][1];
		socket.BF_Data.BlazeVerison 	= blazePacket.packet_decoded["CINF"][1]["DSDK"][1];
		socket.BF_Data.Environment 		= blazePacket.packet_decoded["CINF"][1]["ENV "][1];
		socket.BF_Data.Locale 			= blazePacket.packet_decoded["CINF"][1]["LOC "][1];
		
		//console.log(blazePacket.packet_decoded["CDAT"][1]);
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		responsePacket.writeString("ASRC", "300294");
		responsePacket.writeList("CIDS", [
		1, 25, 4, 27, 28, 6, 7, 9, 10, 63490, 11, 30720, 30721, 30722, 30723, 20, 30725, 30726, 2000
		], Enums.TagType.TYPE_INTEGER)
		
		var configData = [];
		configData["associationListSkipInitialSet"] = "1";
		configData["blazeServerClientId"] 			= "GOS-BlazeServer-BF4-PC";
		configData["bytevaultHostname"] 			= "bytevault.gameservices.ea.com";
		configData["bytevaultPort"] 				= "42210";
		configData["bytevaultSecure"] 				= "false";
		configData["capsStringValidationUri"] 		= "client-strings.xboxlive.com";
		configData["connIdleTimeout"] 				= "90s";
		configData["defaultRequestTimeout"] 		= "60s";
		configData["identityDisplayUri"] 			= "console2/welcome";
		configData["identityRedirectUri"] 			= "http://127.0.0.1/success";
		configData["nucleusConnect"] 				= "http://127.0.0.1";
		configData["nucleusProxy"] 					= "http://127.0.0.1/";
		configData["pingPeriod"] 					= "20s";
		configData["userManagerMaxCachedUsers"] 	= "0";
		configData["voipHeadsetUpdateRate"] 		= "1000";
		configData["xblTokenUrn"] 					= "http://127.0.0.1";
		configData["xlspConnectionIdleTimeout"] 	= "300";
		
		var confData = [];
		confData["CONF"] = [Enums.TagType.TYPE_MAP, configData, Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING];
		
		responsePacket.writeStruct("CONF", confData);
		
		
		responsePacket.writeString("INST", "battlefield-4-pc");
		responsePacket.writeInteger("MINR", 0);
		responsePacket.writeString("NASP", "cem_ea_id");
		responsePacket.writeString("PLAT", "pc");
		
		/////////// QOSS \\\\\\\\\\\
		
		var bwpsData = [];
		bwpsData["PSA"] = [Enums.TagType.TYPE_STRING, "localhost"];
		bwpsData["PSP"] = [Enums.TagType.TYPE_INTEGER, 17502];
		bwpsData["SNA"] = [Enums.TagType.TYPE_STRING, "bio-prod-ams-bf4"];
		
		var qossData = [];
		qossData["BWPS"] = [Enums.TagType.TYPE_STRUCT, bwpsData];
		qossData["LNP "] = [Enums.TagType.TYPE_INTEGER, 10];

		////////// LTPS
		var ltpsData = [];
		
		var amsData = [];
		amsData["PSA "] = [Enums.TagType.TYPE_STRING, "qos-prod-bio-dub-common-common.gos.ea.com"];
		amsData["PSP "] = [Enums.TagType.TYPE_INTEGER, 17502];
		amsData["SNA "] = [Enums.TagType.TYPE_STRING, "bio-prod-ams-bf4"];
		ltpsData["ams"] = amsData;
		
		var gruData = [];
		gruData["PSA "] = [Enums.TagType.TYPE_STRING, "qos-prod-m3d-brz-common-common.gos.ea.com"];
		gruData["PSP "] = [Enums.TagType.TYPE_INTEGER, 17502];
		gruData["SNA "] = [Enums.TagType.TYPE_STRING, "ea-prod-ams-bf4"];
		ltpsData["gru"] = gruData;
		
		var iadData = [];
		iadData["PSA "] = [Enums.TagType.TYPE_STRING, "qos-prod-bio-iad-common-common.gos.ea.com"];
		iadData["PSP "] = [Enums.TagType.TYPE_INTEGER, 17502];
		iadData["SNA "] = [Enums.TagType.TYPE_STRING, "bio-prod-iad-bf4"];
		ltpsData["iad"] = iadData;
		
		var laxData = [];
		laxData["PSA "] = [Enums.TagType.TYPE_STRING, "qos-prod-bio-sjc-common-common.gos.ea.com"];
		laxData["PSP "] = [Enums.TagType.TYPE_INTEGER, 17502];
		laxData["SNA "] = [Enums.TagType.TYPE_STRING, "bio-prod-lax-bf4"];
		ltpsData["lax"] = laxData;
		
		var nrtData = [];
		nrtData["PSA "] = [Enums.TagType.TYPE_STRING, "qos-prod-m3d-nrt-common-common.gos.ea.com"];
		nrtData["PSP "] = [Enums.TagType.TYPE_INTEGER, 17502];
		nrtData["SNA "] = [Enums.TagType.TYPE_STRING, "i3d-prod-nrt-bf4"];
		ltpsData["nrt"] = nrtData;
		
		var sydData = [];
		sydData["PSA "] = [Enums.TagType.TYPE_STRING, "qos-prod-bio-syd-common-common.gos.ea.com"];
		sydData["PSP "] = [Enums.TagType.TYPE_INTEGER, 17502];
		sydData["SNA "] = [Enums.TagType.TYPE_STRING, "bio-prod-syd-bf4"];
		ltpsData["syd"] = sydData;
		
		
		qossData["LTPS"] = [Enums.TagType.TYPE_MAP, ltpsData, Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRUCT];
		
		qossData["SVID"] = [Enums.TagType.TYPE_INTEGER, 1337];
		qossData["TIME"] = [Enums.TagType.TYPE_INTEGER, Math.floor(new Date() / 1000)];
		
		responsePacket.writeStruct("QOSS",qossData);
		
		//////////// END \\\\\\\\\\\\
		
		responsePacket.writeString("RSRC", "302123");
		responsePacket.writeString("SVER", "Blaze 13.15.08.0 (CL# 9442625)");

		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};