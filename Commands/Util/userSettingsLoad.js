// Util Component
// userSettingsLoadAll Command
// Sends the user's settings
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var util			= require('util');

var componentId = 0x0009;
var id 			= 0x000A;
var name 		= "userSettingsLoad";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(util.inspect(blazePacket.packet_decoded, {showHidden: false, depth: null}));
		
		var userID 		= blazePacket.packet_decoded["UID "][1];
		var settingKey	= blazePacket.packet_decoded["KEY "][1];
		
		if(userID == 0 && socket.Blaze_User.getClientType() == Enums.ClientType.CLIENT_TYPE_GAMEPLAY_USER) {
			userID = socket.Blaze_User.getUserID();
		}
		
		var userSettings = [];
		if(userID != 0) {
			var client = sessionManager.getUserById(userID);
			if(client) {
				var settingData = socket.Blaze_User.getUserSetting(settingKey);
				if(settingData !== false) {
					userSettings[settingKey] = settingData;
				}
			}
		}

		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		responsePacket.writeMap("CONF", userSettings, Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};