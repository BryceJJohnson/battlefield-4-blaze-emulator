// Util Component
// ping Command
// Gives the socket the current time.
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x0009;
var id 			= 0x0002;
var name 		= "ping";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.writeInteger("STIM", Math.floor(new Date() / 1000));
		responsePacket.buildPacket();
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};