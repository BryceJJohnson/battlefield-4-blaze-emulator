// Util Component
// postAuth Command
// Gives the socket the required blaze information.
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x0009;
var id 			= 0x0008;
var name 		= "postAuth";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		
		var teleData = [];
		teleData["ADRS"] = [Enums.TagType.TYPE_STRING, "aussieops.net"];
		teleData["ANON"] = [Enums.TagType.TYPE_INTEGER, 0];
		teleData["DISA"] = [Enums.TagType.TYPE_STRING, ""];
		teleData["EDCT"] = [Enums.TagType.TYPE_INTEGER, 0];
		teleData["FILT"] = [Enums.TagType.TYPE_STRING, "-GAME/COMM/EXPD"];
		teleData["LOC "] = [Enums.TagType.TYPE_INTEGER, 1701729619];
		teleData["MINR"] = [Enums.TagType.TYPE_INTEGER, 0];
		teleData["NOOK"] = [Enums.TagType.TYPE_STRING, ""];
		teleData["PORT"] = [Enums.TagType.TYPE_INTEGER, 9988];
		teleData["SDLY"] = [Enums.TagType.TYPE_INTEGER, 15000];
		teleData["SESS"] = [Enums.TagType.TYPE_STRING, "session_key_telemetry"];
		teleData["SKEY"] = [Enums.TagType.TYPE_STRING, "session_key_telemetry"];
		teleData["SPCT"] = [Enums.TagType.TYPE_INTEGER, 75];
		teleData["STIM"] = [Enums.TagType.TYPE_STRING, "Default"];
		teleData["SVNM"] = [Enums.TagType.TYPE_STRING, "telemetry-3-common"];
		responsePacket.writeStruct("TELE", teleData);
		
		var uropData = [];
		uropData["TMOP"] = [Enums.TagType.TYPE_INTEGER, Enums.TelemetryOpt.TELEMETRY_OPT_OUT];
		uropData["UID "] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getUserID()];
		responsePacket.writeStruct("UROP", uropData);

		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};