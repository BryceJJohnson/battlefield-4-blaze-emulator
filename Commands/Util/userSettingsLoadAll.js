// Util Component
// userSettingsLoadAll Command
// Sends the user's settings
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x0009;
var id 			= 0x000C;
var name 		= "userSettingsLoadAll";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		responsePacket.writeMap("CONF", socket.Blaze_User.getUserSettings(), Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING);
		responsePacket.buildPacket();
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};