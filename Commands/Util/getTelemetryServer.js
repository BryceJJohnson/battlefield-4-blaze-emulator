// Util Component
// getTelemetryServer Command
// Gives the socket the telemetry servers details
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x0009;
var id 			= 0x0005;
var name 		= "getTelemetryServer";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		responsePacket.writeString("ADRS", "aussieops.net");
		responsePacket.writeInteger("ANON", 0);
		responsePacket.writeString("DISA", "");
		responsePacket.writeInteger("EDCT", 1);
		responsePacket.writeString("FILT", "-GAME/COMM/EXPD");
		responsePacket.writeInteger("LOC ", 1701729619);
		responsePacket.writeInteger("MINR", 1);
		responsePacket.writeString("NOOK", "");
		responsePacket.writeInteger("PORT", 9988);
		responsePacket.writeInteger("SDLY", 15000);
		responsePacket.writeString("SESS", "session_key_telemetry");
		responsePacket.writeString("SKEY", "session_key_telemetry");
		responsePacket.writeInteger("SPCT", 75);
		responsePacket.writeString("STIM", "Default");
		responsePacket.writeString("SVNM", "telemetry-3-common");

		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};