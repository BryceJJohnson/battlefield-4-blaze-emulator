// Util Component
// userSettingsLoadAll Command
// Sends the user's settings
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');
var util			= require('util');

var componentId = 0x0009;
var id 			= 0x000B;
var name 		= "userSettingsSave";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(util.inspect(blazePacket.packet_decoded, {showHidden: false, depth: null}));
		
		var userID 	= blazePacket.packet_decoded["UID "][1];
		var data 	= blazePacket.packet_decoded["DATA"][1];
		var dataKey	= blazePacket.packet_decoded["KEY "][1];
		
		if(userID == 0 && socket.Blaze_User.getClientType() == Enums.ClientType.CLIENT_TYPE_GAMEPLAY_USER) {
			userID = socket.Blaze_User.getUserID();
		}
		
		if(userID != 0) {
			var client = sessionManager.getUserById(userID);
			if(client)
				client.updateUserSettings(dataKey, data);
		}
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id)
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};