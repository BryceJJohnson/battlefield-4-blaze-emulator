// Authentication2 Component
// loginByAuthCode Command
// Authorizes the socket in the Blaze protocol by the Origin client
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var dataBase 		= require('../../Scripts/databaseManager');
var sessionManager 	= require('../../Scripts/sessionManager');
var BF4Client 		= require('../../Scripts/bf4Client');

var blazeAuth 		= require('../../Scripts/blazeAuthChecker');

var UserAuthenticated 	= require('../../Commands/UserSessions/UserAuthenticated');
var UserAdded 			= require('../../Commands/UserSessions/UserAdded');
var UserUpdated			= require('../../Commands/UserSessions/UserUpdated');

var componentId = 0x0023;
var id 			= 0x000A;
var name 		= "loginByAuthCode";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {		
		if(socket.BF_Data.Type == Enums.ClientType.CLIENT_TYPE_DEDICATED_SERVER) {
			var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, Enums.ErrorCodes.AUTHENTICATION_REQUIRED, Enums.MessageType.ERROR, blazePacket.packet_id);
			responsePacket.buildPacket();
			socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
			return;
		}
		
		var blazeUser = new blazeAuth(blazePacket.packet_decoded["AUTH"][1]);
		blazeUser.then(function(result) {
			if(!result) {
				var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, Enums.ErrorCodes.AUTHENTICATION_REQUIRED, Enums.MessageType.ERROR, blazePacket.packet_id);
				responsePacket.buildPacket();
				socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
				return;
			}
			
			dataBase.getUserByBlazeID(result.packet_decoded["BUID"][1]).then(function(userTable) {
				if(!userTable) {
					var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, Enums.ErrorCodes.AUTHENTICATION_REQUIRED, Enums.MessageType.ERROR, blazePacket.packet_id);
					responsePacket.buildPacket();
					socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
					return;
				}
			
				var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
				
				socket.Blaze_User = new BF4Client(socket, userTable, socket.BF_Data, result);
				
				
				//var connectionGroupID = [30722, 2, socket.Blaze_User.getUserID()];
				//socket.Blaze_User.addBlazeObject(connectionGroupID);
				
				
				responsePacket.writeInteger("ANON", 0);
				responsePacket.writeInteger("NTOS", 0);
				
				var sessData = [];
				sessData["BUID"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getBlazeUserID()];
				//sessData["CGID"] = [Enums.TagType.TYPE_VECTOR3, connectionGroupID, socket.Blaze_User.getUserID()];
				sessData["FRSC"] = [Enums.TagType.TYPE_INTEGER, 0];
				sessData["FRST"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.isFirstLogin()];
				sessData["KEY "] = [Enums.TagType.TYPE_STRING, socket.Blaze_User.getSessionKey()];
				sessData["LLOG"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getLastLogin()];
				sessData["MAIL"] = [Enums.TagType.TYPE_STRING, socket.Blaze_User.getEmail()];
				
				var pdtlData = [];
				pdtlData["DSNM"] = [Enums.TagType.TYPE_STRING, socket.Blaze_User.getDisplayName()];
				pdtlData["PID "] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getUserID()];
				pdtlData["PLAT"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getPlatform()];
				
				sessData["PDTL"] = [Enums.TagType.TYPE_STRUCT, pdtlData];
				
				sessData["UID "] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getUserID()];
				
				responsePacket.writeStruct("SESS", sessData);
				
				responsePacket.writeInteger("SPAM", 0);
				responsePacket.writeInteger("UNDR", 0);
				
				responsePacket.buildPacket();
				socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
				
				//Add session!
				sessionManager.addUser(socket.Blaze_User);
				
				//Send client required information after authing them
				UserAuthenticated.createResponse(socket, socket.Blaze_User);
				UserAdded.createResponse(socket, socket.Blaze_User);
				UserUpdated.createResponse(socket, socket.Blaze_User);
			});
		});
	}
};