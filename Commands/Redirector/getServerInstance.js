// Redirector Component
// getServerInstance Command
// Redirects the connection to the right Blaze server
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var Config 			= require('../../Config');

var componentId = 0x0005;
var id 			= 0x0001;
var name 		= "getServerInstance";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var port = 10041;
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		var valuData = [];
		valuData["HOST"] = [Enums.TagType.TYPE_STRING, Config.ServerIP];
		valuData["IP  "] = [Enums.TagType.TYPE_INTEGER, Config.getServerIPInteger()];
		valuData["PORT"] = [Enums.TagType.TYPE_INTEGER, port];
		
		var addrData = [];
		addrData["VALU"] = [Enums.TagType.TYPE_STRUCT, valuData];
		
		responsePacket.writeUnion("ADDR", addrData, Enums.NetworkAddressMember.MEMBER_XBOXCLIENTADDRESS);
		
		responsePacket.writeInteger("SECU", 0);
		responsePacket.writeInteger("XDNS", 0);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};