// GameReporting Component
// submitTrustedMidGameReport Command
// Stores the mid-game report
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 		= require('../../Scripts/sessionManager');
var ResultNotification 	= require('./ResultNotification');

var fs 		= require('fs-extra')
var path  	= require("path");
var util 	= require('util');


var componentId = 0x001C;
var id 			= 0x0064;
var name 		= "submitTrustedMidGameReport";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(util.inspect(blazePacket, {showHidden: false, depth: null}));
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		var users = blazePacket.packet_decoded["RPRT"][1]["GAME"][1]["PLYR"][1];
		var userIDs = [];
		for(user in users) {
			var userID 			= user.replace("_", "");
			var changedStats	= users[user]["STAT"][1];
			var statData 		= [];
			var statsSpace		= "player_WebPlayerStats";
				
			var statFile 	= path.join(__dirname, "../../BlazeData/user_stats/"+userID+"/"+statsSpace+".txt");		
			fs.readFileSync(statFile).toString().split('\n').forEach(function (line) {
				if(line != '') {
					var lineData =line.replace(/(\r\n|\n|\r)/gm,"").split("=");
					statData[lineData[0]] = lineData[1];
				}
			});
			
			for(stat in changedStats) {
				if(stat in statData) {
					statData[stat] = (parseFloat(statData[stat])+parseFloat(changedStats[stat])).toString();
				}
			}
			
			var stream = fs.createWriteStream(statFile);
			stream.once('open', function(fd) {
				for(stat in statData)
					stream.write(stat+"="+statData[stat]+"\n");
				stream.end();
			});
			
			userIDs.push(parseInt(userID));
		}
		
		var server = sessionManager.getServerByReportId(blazePacket.packet_decoded["GRID"][1]);
		if(server)
			ResultNotification.createResponse(socket, server, userIDs, 0);
	}
};