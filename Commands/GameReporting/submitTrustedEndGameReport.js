// GameReporting Component
// submitTrustedEndGameReport Command
// Stores the end-game report
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 		= require('../../Scripts/sessionManager');
var ResultNotification 	= require('./ResultNotification');

var fs 		= require('fs-extra')
var path  	= require("path");
var util 	= require('util');


var componentId = 0x001C;
var id 			= 0x0065;
var name 		= "submitTrustedEndGameReport";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(util.inspect(blazePacket, {showHidden: false, depth: null}));
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		var server = sessionManager.getServerByReportId(blazePacket.packet_decoded["GRID"][1]);
		if(server) {
			var userIDs = [];
			var roundData = blazePacket.packet_decoded["RPRT"][1]["GAME"][1]["GAME"][1];
			var reportFile 	= path.join(__dirname, '../../BlazeData/game_reports/'+server.getGameType()+'/'+server.getGameReportingID()+'.txt');
			
			var stream = fs.createWriteStream(reportFile);
			stream.once('open', function(fd) {
				for(key in roundData)
					stream.write(key+"="+roundData[key]+"\n");
				stream.end();
			});
			
			ResultNotification.createResponse(socket, server, userIDs, 1);
		}
	}
};