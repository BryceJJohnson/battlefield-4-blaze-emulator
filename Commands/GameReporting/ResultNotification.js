// GameReporting Component
// ResultNotification Command
// Sends the result of a game report
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x001C;
var id 			= 0x0072;
var name 		= "ResultNotification";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server, userList, isFinal) {
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		//TODO Figure out what this is...
		//Maybe game reporting ID?
		//var dataInt = []
		//dataInt.push(1650232724)
		//responsePacket.writeIntegerList("DATA", dataInt);
		
		responsePacket.writeAppend("APPND", "921d21070194b6e4a50c");
		
		var dataData = [];
		if(userList.length > 0)
			dataData["PLYR"] = [Enums.TagType.TYPE_LIST, userList, Enums.TagType.TYPE_INTEGER];
		
		responsePacket.writeStruct("DATA", dataData);
		responsePacket.writeAppend("APPND2", "00");
		
		responsePacket.writeInteger("EROR", 0);
		responsePacket.writeInteger("FNL ", isFinal);
		responsePacket.writeInteger("GHID", server.getGameReportingID()); // TODO Reporting ID
		responsePacket.writeInteger("GRID", server.getGameReportingID());
		responsePacket.buildPacket();
		
		//console.log(responsePacket.packet_complete);
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		if(isFinal == 1)
			server.finishGame();
	}
};