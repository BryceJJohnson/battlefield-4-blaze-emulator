// GameManager Component
// swapPlayers Command
// Changes a players role
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var NotifyGamePlayerTeamRoleSlotChange 	= require('./Async/NotifyGamePlayerTeamRoleSlotChange');

var componentId = 0x0004;
var id 			= 0x0070;
var name 		= "swapPlayers";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		var gameid 	= blazePacket.packet_decoded["GID "][1];
		var server 	= sessionManager.getServerById(gameid);
		var changes = blazePacket.packet_decoded["LGAM"][1];
		if(server) {
			for(var i=0; i<changes.length; i++) {
				NotifyGamePlayerTeamRoleSlotChange.createResponse(server.getSocket(), gameid, changes[i]);
			}
		}
	}
};