// GameManager Component
// leaveGameByGroup Command
// When user leaves a game
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var componentId = 0x0004;
var id 			= 0x0016;
var name 		= "leaveGameByGroup";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(blazePacket.packet_decoded);
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		var server 	= sessionManager.getServerById(blazePacket.packet_decoded["GID "][1]);
		var userID 	= blazePacket.packet_decoded["PID "][1];
		if(userID == 0 && socket.Blaze_User.getClientType() == Enums.ClientType.CLIENT_TYPE_GAMEPLAY_USER) {
			userID = socket.Blaze_User.getUserID();
		}
		
		if(server) {
			var reason = blazePacket.packet_decoded["REAS"][1];
			server.removeUser(userID, reason);
		}
	}
};