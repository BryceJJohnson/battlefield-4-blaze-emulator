// GameManager Component
// createGame Command
// Authorizes a game with the blaze backend
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var BF4Server 		= require('../../Scripts/bf4Server');

var componentId = 0x0004;
var id 			= 0x0001;
var name 		= "createGame";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		
		var ServerObject = new BF4Server(socket, blazePacket.packet_decoded, socket.Blaze_User.getUserID());
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.writeInteger("GID ", ServerObject.getGameID());
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		ServerObject.setUpGame();
	}
};