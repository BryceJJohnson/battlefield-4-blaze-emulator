// GameManager Component
// updateMeshConnection Command
// Updates mesh connection details
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 				= require('../../Scripts/sessionManager');
var NotifyPlayerJoinCompleted	= require('./Async/NotifyPlayerJoinCompleted');

var componentId = 0x0004;
var id 			= 0x001D;
var name 		= "updateMeshConnection";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		//console.log(blazePacket.packet_decoded);
		
		var client = sessionManager.getUserById(blazePacket.packet_decoded["TCG "][1][2]);
		var server = sessionManager.getServerById(blazePacket.packet_decoded["GID "][1]);
		
		if(!client && blazePacket.packet_decoded["STAT"][1] == Enums.PlayerState.DISCONNECTED) {
			
			return
		}
		
		if(!client || !server)
			return false;
		
		client.changePlayerState(blazePacket.packet_decoded);
	}
};