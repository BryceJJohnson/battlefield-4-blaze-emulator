// GameManager Component
// NotifyPlayerJoinCompleted packet
// Notifies a server that a users has finished connecting on Blaze
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x001E;
var name 		= "NotifyPlayerJoinCompleted";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server, user) {
		if(server == false || server === undefined)
			return false;
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("GID ", server.getGameID());
		responsePacket.writeInteger("PID ", user.getUserID());
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};