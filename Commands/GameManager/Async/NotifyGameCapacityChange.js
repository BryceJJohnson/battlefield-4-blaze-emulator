// GameManager Component
// NotifyGameCapacityChange packet
// Notifies a server that the player capacity/info has changed on blaze
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x006F;
var name 		= "NotifyGameCapacityChange";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server) {
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");

		responsePacket.writeList("CAP", server.getPlayerCapacityInfo(), Enums.TagType.TYPE_INTEGER);
		responsePacket.writeInteger("GID", server.getGameID());
		responsePacket.writeStruct("RNFO", server.getRoleInfo());
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};