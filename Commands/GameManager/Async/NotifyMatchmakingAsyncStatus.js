// GameManager Component
// NotifyMatchmakingAsyncStatus packet
// Notifies a user about their matchmaking results
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var NotifyMatchmakingFailed	= require('./NotifyMatchmakingFailed');

var componentId = 0x0004;
var id 			= 0x000C;
var name 		= "NotifyMatchmakingAsyncStatus";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket) {
		
		var games 		= 1;
		var sessionID 	= 0;
		
		//res = 0 (NEW GAME) It Crashes client
		//res = 1 (JOINED NEW GAME) Crashes
		//res = 2 (JOINED EXISTING GAME) Crashes
		//res = 3 (TIMED OUT resetDedicatedServer)
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");	
		
		responsePacket.writeInteger("MSID", sessionID); // Matchmaking Session ID
		responsePacket.writeInteger("USID", socket.Blaze_User.getUserID());
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		NotifyMatchmakingFailed.createResponse(socket, games, sessionID, 3);
	}
};