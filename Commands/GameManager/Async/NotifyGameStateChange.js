// GameManager Component
// NotifyGameStateChange packet
// Notifies a server that the state has been changed on blaze
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var util 			= require('util');

var componentId = 0x0004;
var id 			= 0x0064;
var name 		= "NotifyGameStateChange";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server) {
		if(server == false || server === undefined)
			return false;
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("GID ", server.getGameID());
		responsePacket.writeInteger("GSTA", server.getGameState());
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};