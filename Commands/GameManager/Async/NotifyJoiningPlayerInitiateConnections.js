// GameManager Component
// NotifyJoiningPlayerInitiateConnections packet
// Notifies a user trying to connect to a server about that servers details
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var util = require('util');



var componentId = 0x0004;
var id 			= 0x0016;
var name 		= "NotifyJoiningPlayerInitiateConnections";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server, user) {
		if(server === false || server === undefined || user === false || user === undefined) {
			var responsePacket = new BlazeEncoder(componentId, id, Enums.ErrorCodes.CANCELED, Enums.MessageType.ERROR, "0");
			responsePacket.buildPacket();
			socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
			return false;
		}
		//console.log("STARTING NotifyJoiningPlayerInitiateConnections");
		
		var context 	= Enums.DatalessContext.JOIN_GAME_SETUP_CONTEXT; // Setup multiple context data
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		var gameData = [];
		gameData["ATTR"] = [Enums.TagType.TYPE_MAP, server.getAttributeList(), Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING];
		gameData["CAP "] = [Enums.TagType.TYPE_LIST, server.getPlayerCapacityInfo(), Enums.TagType.TYPE_INTEGER];
		gameData["ESNM"] = [Enums.TagType.TYPE_STRING, "Battlefield_4_Blaze_Emulator"];
		gameData["GID "] = [Enums.TagType.TYPE_INTEGER, server.getGameID()];
		gameData["GMRG"] = [Enums.TagType.TYPE_INTEGER, server.getGameManagerID()];
		gameData["GNAM"] = [Enums.TagType.TYPE_STRING, server.getGameName()];
		gameData["GPVH"] = [Enums.TagType.TYPE_INTEGER, 6667];
		gameData["GSET"] = [Enums.TagType.TYPE_INTEGER, server.getGameSet()];
		gameData["GSID"] = [Enums.TagType.TYPE_INTEGER, server.getGameReportingID()];
		gameData["GSTA"] = [Enums.TagType.TYPE_INTEGER, server.getGameState()];
		gameData["GTYP"] = [Enums.TagType.TYPE_STRING, server.getGameType()];
		gameData["GURL"] = [Enums.TagType.TYPE_STRING, server.getGameURL()];
		gameData["HNET"] = [Enums.TagType.TYPE_LIST, server.getHostNetworkInfo(), Enums.TagType.TYPE_STRUCT];
		gameData["HSES"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["IGNO"] = [Enums.TagType.TYPE_INTEGER, server.getIGNO()];
		gameData["MCAP"] = [Enums.TagType.TYPE_INTEGER, server.getMaxPlayers()];
		gameData["MNCP"] = [Enums.TagType.TYPE_INTEGER, 1];
		gameData["NRES"] = [Enums.TagType.TYPE_INTEGER, server.getNRES()];
		gameData["NTOP"] = [Enums.TagType.TYPE_INTEGER, server.getNetworkTopology()];
		gameData["PGID"] = [Enums.TagType.TYPE_STRING, server.getPersistentID()];
		gameData["PGSR"] = [Enums.TagType.TYPE_BLOB, server.getPersistentIDSecret()];
		
		var phstData = [];
		phstData["CONG"] = [Enums.TagType.TYPE_INTEGER, 0];
		phstData["CSID"] = [Enums.TagType.TYPE_INTEGER, 0];
		phstData["HPID"] = [Enums.TagType.TYPE_INTEGER, server.getHostingPlayerID()];
		phstData["HSLT"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["PHST"] = [Enums.TagType.TYPE_STRUCT, phstData];
		
		gameData["PRES"] = [Enums.TagType.TYPE_INTEGER, server.getPresenceMode()];
		gameData["PSAS"] = [Enums.TagType.TYPE_STRING, server.getPingSite()];
		gameData["QCAP"] = [Enums.TagType.TYPE_INTEGER, server.getMaxQueueSize()];
		gameData["RNFO"] = [Enums.TagType.TYPE_STRUCT, server.getRoleInfo()];
		gameData["SEED"] = [Enums.TagType.TYPE_INTEGER, 0];
		
		var thstData = [];
		thstData["CONG"] = [Enums.TagType.TYPE_INTEGER, 0];
		thstData["CSID"] = [Enums.TagType.TYPE_INTEGER, 0];
		thstData["HPID"] = [Enums.TagType.TYPE_INTEGER, server.getHostingPlayerID()];
		thstData["HSLT"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["THST"] = [Enums.TagType.TYPE_STRUCT, thstData];
		
		gameData["TIDS"] = [Enums.TagType.TYPE_LIST, server.getTeamIDs(), Enums.TagType.TYPE_INTEGER];
		gameData["UUID"] = [Enums.TagType.TYPE_STRING, server.getUUID()];
		gameData["VOIP"] = [Enums.TagType.TYPE_INTEGER, server.getVoipSetting()];
		gameData["VSTR"] = [Enums.TagType.TYPE_STRING, server.getVersion()];
		
		
		responsePacket.writeStruct("GAME", gameData);
		responsePacket.writeInteger("LFPJ", 0);
		
		var userData = [];
		userData["CONG"] = [Enums.TagType.TYPE_INTEGER, user.getUserID()];
		userData["CSID"] = [Enums.TagType.TYPE_INTEGER, user.getCurrentSlotID()];
		userData["EXID"] = [Enums.TagType.TYPE_INTEGER, 0];
		userData["GID "] = [Enums.TagType.TYPE_INTEGER, server.getGameID()];
		userData["LOC "] = [Enums.TagType.TYPE_INTEGER, user.getLocale()];
		userData["NAME"] = [Enums.TagType.TYPE_STRING, user.getDisplayName()];
		userData["PATT"] = [Enums.TagType.TYPE_MAP, user.getJoinData()["ATTR"][1], Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING];
		userData["PID "] = [Enums.TagType.TYPE_INTEGER, user.getUserID()];
		userData["PNET"] = [Enums.TagType.TYPE_UNION, user.getJoinData()["PNET"][1], Enums.NetworkAddressMember.MEMBER_IPPAIRADDRESS];
		
		userData["ROLE"] = [Enums.TagType.TYPE_STRING, "soldier"];
		userData["SID "] = [Enums.TagType.TYPE_INTEGER, user.getCurrentSlotID()];
		userData["SLOT"] = [Enums.TagType.TYPE_INTEGER, user.getJoinData()["SLOT"][1]];
		userData["STAT"] = [Enums.TagType.TYPE_INTEGER, 2];
		userData["TIDX"] = [Enums.TagType.TYPE_INTEGER, 0];
		userData["TIME"] = [Enums.TagType.TYPE_INTEGER, 0];
		userData["UID "] = [Enums.TagType.TYPE_INTEGER,  user.getUserID()];
		
		var prosData = [];
		prosData.push(userData);
		responsePacket.writeList("PROS", prosData, Enums.TagType.TYPE_STRUCT);
		
		var valuData = [];
		valuData["DCTX"] = [Enums.TagType.TYPE_INTEGER, context];
		
		var reasData = [];
		reasData["VALU"] = [Enums.TagType.TYPE_STRUCT, valuData];
		
		responsePacket.writeUnion("REAS", reasData, Enums.NetworkAddressMember.MEMBER_XBOXCLIENTADDRESS);
		
		//console.log(util.inspect(responsePacket.packet_data, {showHidden: false, depth: null}));
		responsePacket.buildPacket();
		//console.log(responsePacket.packet_complete);
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};