// GameManager Component
// NotifyGamePlayerTeamRoleSlotChange packet
// Notifies a server the result of a slot change
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x0075;
var name 		= "NotifyGamePlayerTeamRoleSlotChange";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, gameid, swapInfo) {
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("GID ", gameid);
		responsePacket.writeInteger("PID ", swapInfo["PID "][1]);
		responsePacket.writeString("ROLE", swapInfo["ROLE"][1]);
		responsePacket.writeInteger("SLOT", swapInfo["SLOT"][1]);
		responsePacket.writeInteger("TIDX", swapInfo["TIDX"][1]);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};