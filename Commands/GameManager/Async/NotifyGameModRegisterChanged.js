// GameManager Component
// NotifyGameModRegisterChanged packet
// Notifies a server that a game's mod register has changed
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x007B;
var name 		= "NotifyGameModRegisterChanged";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server, register) {
		if(register == false || register === undefined)
			return false;
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("GMID", server.getGameID());
		responsePacket.writeInteger("GMRG ", register);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};