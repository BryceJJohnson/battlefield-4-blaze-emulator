// GameManager Component
// NotifyPlayerRemoved packet
// Notifies a server that a player has been removed
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x0028;
var name 		= "NotifyPlayerRemoved";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, gameid, userid, reason) {
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("GID ", gameid);
		responsePacket.writeInteger("PID ", userid);
		responsePacket.writeInteger("REAS", reason);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};