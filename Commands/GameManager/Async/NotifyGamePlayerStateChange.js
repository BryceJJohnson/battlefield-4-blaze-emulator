// GameManager Component
// NotifyGamePlayerStateChange packet
// Notifies a server that a users' state has changed
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x0074;
var name 		= "NotifyGamePlayerStateChange";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server, user) {
		if(server == false || server === undefined)
			return false;
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("GID ", server.getGameID());
		responsePacket.writeInteger("PID ", user.getUserID());
		responsePacket.writeInteger("STAT", user.getState());
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};