// GameManager Component
// NotifyMatchmakingFailed packet
// Notifies a user about their matchmaking results
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var util 			= require('util');

var componentId = 0x0004;
var id 			= 0x000A;
var name 		= "NotifyMatchmakingFailed";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, games, sessionID, result) {
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");

		responsePacket.writeInteger("MAXF", games); 	// Max found
		responsePacket.writeInteger("RSLT", result); 	// RESULT
		responsePacket.writeInteger("MSID", sessionID); // Matchmaking Session ID
		responsePacket.writeInteger("USID", socket.Blaze_User.getUserID());
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};