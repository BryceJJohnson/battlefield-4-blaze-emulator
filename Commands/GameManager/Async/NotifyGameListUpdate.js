// GameManager Component
// NotifyGameListUpdate Command
// Sends a game list update
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');
var sessionManager 	= require('../../../Scripts/sessionManager');
var util 			= require('util');

var componentId = 0x0004;
var id 			= 0x00C9;
var name 		= "NotifyGameListUpdate";

var gameListID = 1;

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket) {	
		var server = sessionManager.getServerById(1);
		
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		responsePacket.writeInteger("DONE", 1);
		responsePacket.writeInteger("GLID", gameListID);
		
		if(server) {
			var updateList = [];
			var updateData = [];
			updateData["FIT "] = [Enums.TagType.TYPE_INTEGER, 1];
			
			var gameData = [];
			
			gameData["ATTR"] = [Enums.TagType.TYPE_MAP, server.getAttributeList(), Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING];
			gameData["CAP "] = [Enums.TagType.TYPE_LIST, server.getPlayerCapacityInfo(), Enums.TagType.TYPE_INTEGER];
			gameData["GID "] = [Enums.TagType.TYPE_INTEGER, server.getGameID()];
			gameData["GMRG"] = [Enums.TagType.TYPE_INTEGER, server.getGameManagerID()];
			gameData["GNAM"] = [Enums.TagType.TYPE_STRING, server.getGameName()];
			gameData["GSET"] = [Enums.TagType.TYPE_INTEGER, server.getGameSet()];
			gameData["GSTA"] = [Enums.TagType.TYPE_INTEGER, server.getGameState()];
			gameData["GURL"] = [Enums.TagType.TYPE_STRING, server.getGameURL()];
			gameData["HNET"] = [Enums.TagType.TYPE_LIST, server.getHostNetworkInfo(), Enums.TagType.TYPE_STRUCT];
			gameData["HOST"] = [Enums.TagType.TYPE_INTEGER, server.getHostingPlayerID()];
			gameData["HSES"] = [Enums.TagType.TYPE_INTEGER, 13666];
			gameData["NTOP"] = [Enums.TagType.TYPE_INTEGER, server.getNetworkTopology()];
			gameData["PCNT"] = [Enums.TagType.TYPE_LIST, [0, 0, 0, 0], Enums.TagType.TYPE_INTEGER];
			gameData["PRES"] = [Enums.TagType.TYPE_INTEGER, server.getPresenceMode()];
			gameData["PSAS"] = [Enums.TagType.TYPE_STRING, server.getPingSite()];
			gameData["PSID"] = [Enums.TagType.TYPE_STRING, server.getPersistentID()];
			gameData["QCAP"] = [Enums.TagType.TYPE_INTEGER, server.getMaxQueueSize()];
			gameData["QCNT"] = [Enums.TagType.TYPE_INTEGER, 0];
			gameData["RNFO"] = [Enums.TagType.TYPE_STRUCT, server.getRoleInfo()];
			
			//Each PlayerInfo
			//gameData["ROST"] = [Enums.TagType.TYPE_LIST, [], Enums.TagType.TYPE_STRUCT];
			
			gameData["SID"] = [Enums.TagType.TYPE_INTEGER, 0];
			gameData["TCAP"] = [Enums.TagType.TYPE_INTEGER, 66];
			
			var teamInfo = [];
			var data = [];
			var roleInfo = [];
			roleInfo["commander"] = 2;
			roleInfo["solider"]  = 64;
			
			data["RMAP"] = [Enums.TagType.TYPE_MAP, roleInfo, Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_INTEGER];
			data["TID "] = [Enums.TagType.TYPE_INTEGER, 65534];
			data["TSZE"] = [Enums.TagType.TYPE_INTEGER, 66];
			teamInfo.push(data);
			
			gameData["TINF"] = [Enums.TagType.TYPE_LIST, teamInfo, Enums.TagType.TYPE_STRUCT];
		
			
			gameData["VOIP"] = [Enums.TagType.TYPE_INTEGER, server.getVoipSetting()];
			gameData["VSTR"] = [Enums.TagType.TYPE_STRING, server.getVersion()];
			
			updateData["GAM "] = [Enums.TagType.TYPE_STRUCT, gameData];
			
			updateList.push(updateData);
			responsePacket.writeList("UPDT", updateList, Enums.TagType.TYPE_STRUCT);
		}
		
		
		responsePacket.buildPacket();
		//console.log(responsePacket.packet_complete);
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		gameListID++;
	}
};