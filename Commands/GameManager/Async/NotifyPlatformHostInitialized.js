// GameManager Component
// NotifyPlatformHostInitialized packet
// Notifies a server/user that the server is Live on Blaze.
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x0047;
var name 		= "NotifyPlatformHostInitialized";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, gameid, userid, slot) {
		if(slot == false || slot === undefined)
			slot = 0;
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("GID ", gameid);
		responsePacket.writeInteger("PHID", userid);
		responsePacket.writeInteger("PHST", slot);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};