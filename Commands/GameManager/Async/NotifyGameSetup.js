// GameManager Component
// NotifyGameSetup packet
// Notifies a user/server the game has been authorized and created on blaze
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x0014;
var name 		= "NotifyGameSetup";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server) {
		if(server == false || server === undefined)
			return false;
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		var gameData = [];
		gameData["ATTR"] = [Enums.TagType.TYPE_MAP, server.getAttributeList(), Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING];
		gameData["CAP "] = [Enums.TagType.TYPE_LIST, server.getPlayerCapacityInfo(), Enums.TagType.TYPE_INTEGER];
		gameData["ESNM"] = [Enums.TagType.TYPE_STRING, "Battlefield_4_Blaze_Emulator"];
		gameData["GID "] = [Enums.TagType.TYPE_INTEGER, server.getGameID()];
		gameData["GMRG"] = [Enums.TagType.TYPE_INTEGER, server.getGameManagerID()];
		gameData["GNAM"] = [Enums.TagType.TYPE_STRING, server.getGameName()];
		gameData["GPVH"] = [Enums.TagType.TYPE_INTEGER, 6667];
		gameData["GSET"] = [Enums.TagType.TYPE_INTEGER, server.getGameSet()];
		gameData["GSID"] = [Enums.TagType.TYPE_INTEGER, server.getGameReportingID()];
		gameData["GSTA"] = [Enums.TagType.TYPE_INTEGER, server.getGameState()];
		gameData["GTYP"] = [Enums.TagType.TYPE_STRING, server.getGameType()];
		gameData["GURL"] = [Enums.TagType.TYPE_STRING, server.getGameURL()];
		gameData["HNET"] = [Enums.TagType.TYPE_LIST, server.getHostNetworkInfo(), Enums.TagType.TYPE_STRUCT];
		gameData["HSES"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["IGNO"] = [Enums.TagType.TYPE_INTEGER, server.getIGNO()];
		gameData["MCAP"] = [Enums.TagType.TYPE_INTEGER, server.getMaxPlayers()];

		//ADD PHST Structure
		var nqosData = []
		nqosData["DBPS"] = [Enums.TagType.TYPE_INTEGER, 0];
		nqosData["NATT"] = [Enums.TagType.TYPE_INTEGER, 0];
		nqosData["UBPS"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["NQOS"] = [Enums.TagType.TYPE_STRUCT, nqosData];

		gameData["NRES"] = [Enums.TagType.TYPE_INTEGER, 1];
		gameData["NTOP"] = [Enums.TagType.TYPE_INTEGER, 1];
		gameData["OGHI"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["PGID"] = [Enums.TagType.TYPE_STRING, server.getPersistentID()];
		gameData["PGSR"] = [Enums.TagType.TYPE_BLOB, server.getPersistentIDSecret()]; // ToDo - Write BuildBlob
		
		//ADD PHST Structure
		var phstData = []
		phstData["CONG"] = [Enums.TagType.TYPE_INTEGER, server.getConnectionID()];
		phstData["CSID"] = [Enums.TagType.TYPE_INTEGER, 0];
		phstData["HPID"] = [Enums.TagType.TYPE_INTEGER, server.getHostingPlayerID()];
		phstData["HSLT"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["PHST"] = [Enums.TagType.TYPE_STRUCT, phstData];

		gameData["PRES"] = [Enums.TagType.TYPE_INTEGER, server.getPresenceMode()];
		gameData["PSAS"] = [Enums.TagType.TYPE_STRING, server.getPingSite()];
		gameData["QCAP"] = [Enums.TagType.TYPE_INTEGER, server.getMaxQueueSize()];
		gameData["RNFO"] = [Enums.TagType.TYPE_STRUCT, server.getRoleInfo()];
		gameData["SEED"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["STMN"] = [Enums.TagType.TYPE_STRING, ""];
		
		//ADD THST Structure
		var thstData = []
		phstData["CONG"] = [Enums.TagType.TYPE_INTEGER, server.getConnectionID()];
		phstData["CSID"] = [Enums.TagType.TYPE_INTEGER, 0];
		phstData["HPID"] = [Enums.TagType.TYPE_INTEGER, server.getHostingPlayerID()];
		phstData["HSLT"] = [Enums.TagType.TYPE_INTEGER, 0];
		gameData["THST"] = [Enums.TagType.TYPE_STRUCT, phstData];
		
		gameData["TIDS"] = [Enums.TagType.TYPE_LIST, server.getTeamIDs(), Enums.TagType.TYPE_INTEGER];
		gameData["UUID"] = [Enums.TagType.TYPE_STRING, server.getUUID()];
		gameData["VOIP"] = [Enums.TagType.TYPE_INTEGER, server.getVoipSetting()];
		gameData["VSTR"] = [Enums.TagType.TYPE_STRING, server.getVersion()];
		
		responsePacket.writeStruct("GAME", gameData);
		
		var valuData = [];
		valuData["DCTX"] = [Enums.TagType.TYPE_INTEGER, Enums.DatalessContext.CREATE_GAME_SETUP_CONTEXT];
		
		var reasData = [];
		reasData["VALU"] = [Enums.TagType.TYPE_STRUCT, valuData];
		
		responsePacket.writeUnion("REAS", reasData, Enums.NetworkAddressMember.MEMBER_XBOXCLIENTADDRESS);
		
		responsePacket.buildPacket();
		//console.log(responsePacket.packet_complete);
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};