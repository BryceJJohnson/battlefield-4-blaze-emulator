// GameManager Component
// NotifyPlayerClaimingReservation packet
// Notifies a server that user is claiming a reservation to connect.
var BlazeEncoder 	= require('../../../Protocol/Encoder');
var Enums 			= require('../../../Protocol/Enums');

var componentId = 0x0004;
var id 			= 0x0019;
var name 		= "NotifyPlayerClaimingReservation";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, server, user) {
		if(server === false || server === undefined || user === false || user === undefined) {
			var responsePacket = new BlazeEncoder(componentId, id, Enums.ErrorCodes.CANCELED, Enums.MessageType.ERROR, "0");
			responsePacket.buildPacket();
			socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
			return false;
		}
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		responsePacket.writeInteger("GID ", server.getGameID()); // ADD GID
		
		var pdatData = [];
		pdatData["BLOB"] = [Enums.TagType.TYPE_BLOB, ""];
		pdatData["CONG"] = [Enums.TagType.TYPE_INTEGER, user.getUserID()]; // ADD CONG
		pdatData["CSID"] = [Enums.TagType.TYPE_INTEGER, user.getCurrentSlotID()];
		pdatData["EXID"] = [Enums.TagType.TYPE_INTEGER, 0];
		pdatData["GID "] = [Enums.TagType.TYPE_INTEGER, server.getGameID()]; // ADD GID
		pdatData["JFPS"] = [Enums.TagType.TYPE_INTEGER, 0]; // ADD GID
		pdatData["LOC "] = [Enums.TagType.TYPE_INTEGER, user.getLocale()];
		pdatData["NAME"] = [Enums.TagType.TYPE_STRING, user.getDisplayName()];
		pdatData["PATT"] = [Enums.TagType.TYPE_MAP, user.getJoinData()["ATTR"][1], Enums.TagType.TYPE_STRING, Enums.TagType.TYPE_STRING];
		pdatData["PID "] = [Enums.TagType.TYPE_INTEGER, user.getUserID()];
		pdatData["PNET"] = [Enums.TagType.TYPE_UNION, user.getJoinData()["PNET"][1], Enums.NetworkAddressMember.MEMBER_IPPAIRADDRESS];
		
		pdatData["ROLE"] = [Enums.TagType.TYPE_STRING, "soldier"];
		pdatData["SID "] = [Enums.TagType.TYPE_INTEGER, user.getCurrentSlotID()];
		pdatData["SLOT"] = [Enums.TagType.TYPE_INTEGER, user.getJoinData()["SLOT"][1]];
		pdatData["STAT"] = [Enums.TagType.TYPE_INTEGER, 2];
		pdatData["TIDX"] = [Enums.TagType.TYPE_INTEGER, 0];
		pdatData["TIME"] = [Enums.TagType.TYPE_INTEGER, 0];
		//pdatData["UGID"] = [Enums.TagType.TYPE_VECTOR3, [30722, 2, user.getUserID()]];
		pdatData["UID "] = [Enums.TagType.TYPE_INTEGER, user.getUserID()];
		
		responsePacket.writeStruct("PDAT", pdatData);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};