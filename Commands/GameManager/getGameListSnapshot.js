// GameManager Component
// getGameListSnapshot Command
// Gets game data for a specific user
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');
var util 			= require('util');

var NotifyGameListUpdate	= require('./Async/NotifyGameListUpdate');

var componentId = 0x0004;
var id 			= 0x0064;
var name 		= "getGameListSnapshot";

var gameListID = 1;

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {	
		//console.log(util.inspect(blazePacket.packet_decoded, {showHidden: false, depth: null}));
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		responsePacket.writeInteger("GLID", gameListID);
		responsePacket.writeInteger("MAXF", 1);
		responsePacket.writeInteger("NGD ", 1);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		NotifyGameListUpdate.createResponse(socket);
		gameListID++;
	}
};