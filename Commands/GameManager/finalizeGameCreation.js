// GameManager Component
// finalizeGameCreation Command
// Tells Server that game is finished creating
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var componentId = 0x0004;
var id 			= 0x000F;
var name 		= "finalizeGameCreation";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
				
		var server = sessionManager.getServerById(blazePacket.packet_decoded["GID "][1]);
		server.finishCreating();
	}
};