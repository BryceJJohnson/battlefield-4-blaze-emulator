// GameManager Component
// joinGame Command
// Setups the client and the server for the joining process
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var util 	= require('util');

var sessionManager 	= require('../../Scripts/sessionManager');

var UserAdded 								= require('../UserSessions/UserAdded');
var UserUpdated								= require('../UserSessions/UserUpdated');
var UserSessionExtendedDataUpdate 			= require('../UserSessions/UserSessionExtendedDataUpdate');
var NotifyPlayerJoining 					= require('./Async/NotifyPlayerJoining');
var NotifyJoiningPlayerInitiateConnections 	= require('./Async/NotifyJoiningPlayerInitiateConnections');
var NotifyPlayerClaimingReservation 		= require('./Async/NotifyPlayerClaimingReservation');
var NotifyPlatformHostInitialized 			= require('./Async/NotifyPlatformHostInitialized');

var util = require('util');

var componentId = 0x0004;
var id 			= 0x0009;
var name 		= "joinGame";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(util.inspect(blazePacket, {showHidden: false, depth: null}));
		var server = sessionManager.getServerById(blazePacket.packet_decoded["GID "][1]);
		if(!server) {
			// Server not found!
			var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, Enums.ErrorCodes.SYSTEM, Enums.MessageType.ERROR, blazePacket.packet_id);
			responsePacket.buildPacket();
			socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
			return;
		}
		var clientSlotID = server.playerJoinFromType(socket.Blaze_User.getUserID(), blazePacket.packet_decoded["SLOT"][1]);
		socket.Blaze_User.setCurrentSlotID(clientSlotID);
		
		//blazePacket.packet_decoded["PNET"][1]["VALU"][1]["MACI"][1] = 0;
		blazePacket.packet_decoded["PNET"][1] = socket.Blaze_User.getNetworkInfo();
		socket.Blaze_User.setJoinData(blazePacket.packet_decoded);
		
		var entryType 		= blazePacket.packet_decoded["GENT"][1];
		var responsePacket 	= new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		responsePacket.writeInteger("GID ", blazePacket.packet_decoded["GID "][1])
		responsePacket.writeInteger("JGS ", 0)
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		// Add stuff to server
		//NotifyPlatformHostInitialized.createResponse(socket, server.getGameID(), socket.Blaze_User.getUserID(), 0);
		//NotifyPlatformHostInitialized.createResponse(server.getSocket(), server.getGameID(), socket.Blaze_User.getUserID(), 1);
		
		//Add Each user to each other
		UserAdded.createResponse(socket, server.getSocket().Blaze_User);						//CLIENT
		UserUpdated.createResponse(socket, server.getSocket().Blaze_User);						//CLIENT
		UserSessionExtendedDataUpdate.createResponse(socket, server.getSocket().Blaze_User);	//CLIENT
		
		UserAdded.createResponse(server.getSocket(), socket.Blaze_User);
		UserUpdated.createResponse(server.getSocket(), socket.Blaze_User);
		UserSessionExtendedDataUpdate.createResponse(server.getSocket(), socket.Blaze_User);
		
		//Final Server Stuff
		NotifyPlayerJoining.createResponse(server.getSocket(), server, socket.Blaze_User);
		NotifyJoiningPlayerInitiateConnections.createResponse(socket, server, socket.Blaze_User);
		
		
		// Add stuff to client
		
		//UserSessionExtendedDataUpdate.createResponse(socket, socket.Blaze_User, true);
		
		

		
		//UserSessionExtendedDataUpdate.createResponse(server.getSocket(), socket.Blaze_User, true);
		
		//NotifyPlayerClaimingReservation.createResponse(server.getSocket(), server, socket.Blaze_User);
		
		
		// Final Server
		// -- TODO 0004 0015 -- NotifyPlayerJoining
		
		
		if(entryType == Enums.GameEntryType.GAME_ENTRY_TYPE_CLAIM_RESERVATION) {
			// Finally tell the server the player is claiming a reservation
			// -- TODO 0004 0019 -- NotifyPlayerClaimingReservation 
		}
	}
};