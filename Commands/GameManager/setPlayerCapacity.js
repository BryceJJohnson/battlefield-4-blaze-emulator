// GameManager Component
// setPlayerCapacity Command
// Changes a server's player capacity on Blaze
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var componentId = 0x0004;
var id 			= 0x0005;
var name 		= "setPlayerCapacity";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
	
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		var server = sessionManager.getServerById(blazePacket.packet_decoded["GID "][1]);
		if(server)
			server.changeCapacity(blazePacket.packet_decoded["PCAP"][1], blazePacket.packet_decoded["RNFO"][1]);
	}
};