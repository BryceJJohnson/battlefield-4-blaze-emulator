// Packs Component
// getPacks Command
// Sends the user's battlepacks
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var componentId = 0x0802;
var id 			= 0x0001;
var name 		= "getPacks";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		
		var userID 	= blazePacket.packet_decoded["UID "][1];
		if(userID == 0 && socket.Blaze_User.getClientType() == Enums.ClientType.CLIENT_TYPE_GAMEPLAY_USER) {
			userID = socket.Blaze_User.getUserID();
		}
		
		//Todo Error checking
		var client = sessionManager.getUserById(userID);
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		var packListData = [];
		var packs = client.getPacks();
		
		for(var packIndex = 0; packIndex<packs.length; packIndex++) {
			var packStruct = [];
			//console.log("PACK INDEX = " + packIndex);
			//Todo Items in pack
			if(packs[packIndex][2].length == 0) {
					//packStruct["ITLI"] = [Enums.TagType.TYPE_LIST, packs[packIndex][2], Enums.TagType.TYPE_STRING];
				packStruct["PID "] = [Enums.TagType.TYPE_INTEGER, packIndex];
				packStruct["PKEY"] = [Enums.TagType.TYPE_STRING, packs[packIndex][0]];
				packStruct["SCAT"] = [Enums.TagType.TYPE_INTEGER, packs[packIndex][1]];
				packStruct["TGEN"] = [Enums.TagType.TYPE_INTEGER, 0];
				packStruct["TVAL"] = [Enums.TagType.TYPE_INTEGER, 0];
				packStruct["UID "] = [Enums.TagType.TYPE_INTEGER, userID];
				
				packListData.push(packStruct);
			}
		}
		
		responsePacket.writeList("PLST", packListData, Enums.TagType.TYPE_STRUCT);
		
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};