// Packs Component
// openPack Command
// Opens a pack
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var itemList 				= require('../../Commands/Inventory/getTemplate').getItemList();
var PackOpenedNotification 	= require('./PackOpenedNotification');

var componentId = 0x0802;
var id 			= 0x0004;
var name 		= "openPack";

function getRandomIntInclusive(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(blazePacket);
		
		var user 		= socket.Blaze_User;
		var usersItems	= user.getItems();
		var packIndex 	= blazePacket.packet_decoded["PID "][1];
		var itemsAmount = getRandomIntInclusive(2, 5);
		var packItems	= [];
		
		//console.log(itemsAmount);
		//Todo boosts
		for(var i=0; i<itemsAmount; i++) {
			//Item Limit Reached
			if((usersItems.length + packItems.length) >= 2223)
				break;
			
			var itemID = "";
			while (itemID == "") {
				selectedItem = itemList[getRandomIntInclusive(0,2223)];
				if(!user.hasItem(selectedItem)) {
					itemID = selectedItem;
				}
			}
			
			packItems.push(itemID);
		}

		user.setPackItems(packIndex, packItems);
		var userPack = user.getPacks()[packIndex];
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.writeList("ITLI", packItems, Enums.TagType.TYPE_STRING);
		responsePacket.writeInteger("PID ", packIndex);
		responsePacket.writeString("PKEY", userPack[0]);
		responsePacket.writeInteger("SCAT", userPack[1]);
		responsePacket.writeInteger("TGEN", 0);
		responsePacket.writeInteger("TVAL", 0);
		responsePacket.writeInteger("UID ", user.getUserID());
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		PackOpenedNotification.createResponse(socket, user, packIndex, userPack);
		if(user.getCurrentGameID() != 0) {
			var server = sessionManager.getServerById(user.getCurrentGameID());
			if(server) {
				PackOpenedNotification.createResponse(server.getSocket(), user, packIndex, userPack);
			}
		}
	}
};