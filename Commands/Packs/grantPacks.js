// Packs Component
// grantPacks Command
// Server telling blaze a user has unlocked specified packs
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var sessionManager 	= require('../../Scripts/sessionManager');

var componentId = 0x0802;
var id 			= 0x0002;
var name 		= "grantPacks";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		
		//Offline granting of packs?
		var userID 		= blazePacket.packet_decoded["UID "][1];
		var packList 	= blazePacket.packet_decoded["PKLS"][1];
		var awardType	= blazePacket.packet_decoded["SCAT"][1];
		var client 		= sessionManager.getUserById(userID);
		
		if(client) {
			for(pack in packList) {
				client.givePack(packList[pack], awardType)
			}
		}
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		var pidlData = [];
		
		for(pack in packList) {
			var packData = [];
			packData["ERR "] = [Enums.TagType.TYPE_INTEGER, 0];
			packData["PKEY"] = [Enums.TagType.TYPE_STRING, packList[pack]];
			pidlData.push(packData);
		}
		
		responsePacket.writeList("PIDL", pidlData, Enums.TagType.TYPE_STRUCT);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};