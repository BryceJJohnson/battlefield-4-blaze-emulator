// Packs Component
// PackOpenedNotification packet
// Notifies a user about their matchmaking results
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x0802;
var id 			= 0x000A;
var name 		= "PackOpenedNotification";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, user, packID, packData) {
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");

		responsePacket.writeList("ITLI", packData[2], Enums.TagType.TYPE_STRING);
		responsePacket.writeInteger("PID ", packID);
		responsePacket.writeString("PKEY", packData[0]);
		responsePacket.writeInteger("SCAT", packData[1]);
		responsePacket.writeInteger("TGEN", 0);
		responsePacket.writeInteger("TVAL", 0);
		responsePacket.writeInteger("UID ", user.getUserID());
		responsePacket.buildPacket();
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};