// Authentication Component
// getLegalDocsInfo Command
// Authorizes the socket in the Blaze protocol
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var dataBase 		= require('../../Scripts/databaseManager');
var sessionManager 	= require('../../Scripts/sessionManager');

var componentId = 0x0001;
var id 			= 0x00F2;
var name 		= "getLegalDocsInfo";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		console.log(blazePacket);
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};