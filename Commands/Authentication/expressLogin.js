// Authentication Component
// expressLogin Command
// Authorizes the socket in the Blaze protocol
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var dataBase 		= require('../../Scripts/databaseManager');
var sessionManager 	= require('../../Scripts/sessionManager');
var BF4Client 		= require('../../Scripts/bf4Client');

var UserAuthenticated 	= require('../../Commands/UserSessions/UserAuthenticated');
var UserAdded 			= require('../../Commands/UserSessions/UserAdded');
var UserUpdated			= require('../../Commands/UserSessions/UserUpdated');

var componentId = 0x0001;
var id 			= 0x003C;
var name 		= "expressLogin";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		//console.log(blazePacket);
		
		var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		
		var serverUser = {
			BlazeUserID: 10000,
			UserID: 10000,
			DisplayName: "bf4-pc",
			EMail: "bf4.server.pc@ea.com",
			platform: 4,
			LastLogin: 0,
			FirstLogin: 0,
			Status: 2,
			UserSettings: ""
		};
		
		if(socket.Blaze_User === undefined)
			socket.Blaze_User = {};
		socket.Blaze_User = new BF4Client(socket, serverUser, socket.BF_Data);
		sessionManager.addUser(socket.Blaze_User);
		
		responsePacket.writeInteger("AGUP", 0);
		responsePacket.writeInteger("ANON", 0);
		responsePacket.writeInteger("NTOS", 0);
		
		responsePacket.writeString("PCTK", "PlayerTicket_1337");
		
		var connectionGroupID = [30722, 2, 0];
		//socket.Blaze_User.addBlazeObject(connectionGroupID);
		
		var sessData = [];
		sessData["BUID"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getBlazeUserID()];
		sessData["CGID"] = [Enums.TagType.TYPE_VECTOR3, connectionGroupID, socket.Blaze_User.getUserID()];
		sessData["FRST"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.isFirstLogin()];
		sessData["KEY "] = [Enums.TagType.TYPE_STRING, socket.Blaze_User.getSessionKey()];
		sessData["LLOG"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getLastLogin()];
		sessData["MAIL"] = [Enums.TagType.TYPE_STRING, socket.Blaze_User.getEmail()];
		
		var pdtlData = [];
		pdtlData["DSNM"] = [Enums.TagType.TYPE_STRING, socket.Blaze_User.getDisplayName()];
		pdtlData["LAST"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getLastLogin()];
		pdtlData["PID "] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getUserID()];
		pdtlData["PLAT"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getPlatform()];
		pdtlData["STAS"] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getAccountStatus()];
		pdtlData["XREF"] = [Enums.TagType.TYPE_INTEGER, 0];
		
		sessData["PDTL"] = [Enums.TagType.TYPE_STRUCT, pdtlData];
		
		sessData["UID "] = [Enums.TagType.TYPE_INTEGER, socket.Blaze_User.getUserID()];
		
		responsePacket.writeStruct("SESS", sessData);
		
		responsePacket.writeInteger("SPAM", 0)
		responsePacket.writeInteger("UNDR", 0)
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		UserAuthenticated.createResponse(socket, socket.Blaze_User);
		UserAdded.createResponse(socket, socket.Blaze_User);
		UserUpdated.createResponse(socket, socket.Blaze_User);
	}
};