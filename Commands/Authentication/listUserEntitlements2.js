// Authentication Component
// listUserEntitlements2 Command
// Sends the requested users entitlements
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var dataBase 		= require('../../Scripts/databaseManager');
var sessionManager 	= require('../../Scripts/sessionManager');

var componentId = 0x0001;
var id 			= 0x001D;
var name 		= "listUserEntitlements2";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		dataBase.getGlobalEntitlements().then(function(globalEntitlements) {
			//console.log(globalEntitlements);
			var responsePacket = new BlazeEncoder(blazePacket.packet_component, blazePacket.packet_command, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
			if(globalEntitlements) {				
				var nlstData = [];
				for(var i=0; i < globalEntitlements.length; i++) {
					var entitlementData = [];
					entitlementData["DEVI"] = [Enums.TagType.TYPE_STRING, ""];
					entitlementData["GDAY"] = [Enums.TagType.TYPE_STRING, ""];
					entitlementData["GNAM"] = [Enums.TagType.TYPE_STRING, globalEntitlements[i].GroupName];
					entitlementData["ID  "] = [Enums.TagType.TYPE_INTEGER, globalEntitlements[i].EntitlementID];
					entitlementData["ISCO"] = [Enums.TagType.TYPE_INTEGER, 0];
					entitlementData["PID "] = [Enums.TagType.TYPE_INTEGER, 0];
					entitlementData["PJID"] = [Enums.TagType.TYPE_STRING, ""];
					entitlementData["PRCA"] = [Enums.TagType.TYPE_INTEGER, 2];
					entitlementData["PRID"] = [Enums.TagType.TYPE_STRING, globalEntitlements[i].ProductID];
					entitlementData["STAT"] = [Enums.TagType.TYPE_INTEGER, 1];
					entitlementData["STRC"] = [Enums.TagType.TYPE_INTEGER, 0];
					entitlementData["TAG "] = [Enums.TagType.TYPE_STRING, globalEntitlements[i].Tag];
					entitlementData["TDAY"] = [Enums.TagType.TYPE_STRING, ""];
					entitlementData["TYPE"] = [Enums.TagType.TYPE_INTEGER, globalEntitlements[i].Type];
					entitlementData["UCNT"] = [Enums.TagType.TYPE_INTEGER, 0];
					entitlementData["VER "] = [Enums.TagType.TYPE_INTEGER, 0];
					nlstData.push(entitlementData);
				}
				responsePacket.writeList("NLST", nlstData, Enums.TagType.TYPE_STRUCT);
			}
			responsePacket.buildPacket();
			socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		});
	}
};