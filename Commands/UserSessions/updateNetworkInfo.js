// UserSessions Component
// updateNetworkInfo packet
// Updates blaze on the network information of a socket.
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var util = require('util');

var componentId = 0x7802;
var id 			= 0x0014;
var name 		= "updateNetworkInfo";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		var addr 	= socket.remoteAddress;
		var d 		= addr.split('.');
		var ip 		= ((((((+d[0])*256)+(+d[1]))*256)+(+d[2]))*256)+(+d[3]);
		
		blazePacket.packet_decoded["ADDR"][1]["VALU"][1]["EXIP"][1]["IP  "][1] = ip;
		blazePacket.packet_decoded["ADDR"][1]["VALU"][1]["EXIP"][1]["PORT"][1] = blazePacket.packet_decoded["ADDR"][1]["VALU"][1]["INIP"][1]["PORT"][1];
		//blazePacket.packet_decoded["ADDR"][1]["VALU"][1]["MACI"][1] = 0;
		
		//console.log(util.inspect(blazePacket.packet_decoded, {showHidden: false, depth: null}));
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		//console.log(util.inspect(blazePacket.packet_decoded, {showHidden: false, depth: null}));
		
		socket.Blaze_User.updateNetworkInfo(blazePacket.packet_decoded);
	}
};