// UserSessions Component
// UserSessionExtendedDataUpdate packet
// Notifies a server/user that a users network information has changed.
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x7802;
var id 			= 0x0001;
var name 		= "UserSessionExtendedDataUpdate";

var util            = require('util');

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, user, userlist) {
		if(userlist === undefined)
			userlist = false;
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		var dataData = [];
		
		dataData["ADDR"] = [Enums.TagType.TYPE_UNION, user.getNetworkInfo(), Enums.NetworkAddressMember.MEMBER_IPPAIRADDRESS];
		//Best pingsite alias
		dataData["BPS "] = [Enums.TagType.TYPE_STRING, "syd"];

		//Unknown what this does
		/*
		var dmapData = [];
		dmapData[458753] = 37;
		dmapData[458754] = 182;
		dmapData[2013396993] = 0;
		dataData["DMAP"] = [Enums.TagType.TYPE_MAP, dmapData, Enums.TagType.TYPE_INTEGER, Enums.TagType.TYPE_INTEGER];
		*/
		//Hardware Flags
		dataData["HWFG"] = [Enums.TagType.TYPE_INTEGER, 0];
		
		//Ping site list # in ms
		dataData["PSLM"] = [Enums.TagType.TYPE_LIST, user.getPingSiteList(), Enums.TagType.TYPE_INTEGER];
		
		//Quality of Service data
		dataData["QDAT"] = [Enums.TagType.TYPE_STRUCT, user.getNetworkQuality()];

		//mUserInfoAttribute -- DogTags
		dataData["UATT"] = [Enums.TagType.TYPE_INTEGER, user.getAttribute()];
		
		//Blaze Object List
		//dataData["ULST"] = [Enums.TagType.TYPE_LIST, user.getBlazeObjects(), Enums.TagType.TYPE_VECTOR3];
		
		responsePacket.writeStruct("DATA", dataData);
		
		responsePacket.writeInteger("SUBS", 1);					//IS SERVER SUBSCRIBED TO USER UPDATES
		responsePacket.writeInteger("USID", user.getUserID());
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};