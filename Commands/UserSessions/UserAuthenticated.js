// UserSessions Component
// UserAuthenticated packet
// Sent when blaze has authenticated a user
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x7802;
var id 			= 0x0008;
var name 		= "UserAuthenticated";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, user) {
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		responsePacket.writeInteger("ALOC", user.getLocale());
		responsePacket.writeInteger("BUID", user.getBlazeUserID());
		responsePacket.writeVector3("CGID", [30722, 2, 1]);
		responsePacket.writeString("DSNM", user.getDisplayName());
		responsePacket.writeInteger("FRSC", 0);
		responsePacket.writeInteger("FRST", user.isFirstLogin());
		responsePacket.writeString("KEY ", user.getSessionKey());
		responsePacket.writeInteger("LAST", user.getLastLogin());
		responsePacket.writeInteger("LLOG", user.getLastLogin());
		responsePacket.writeString("MAIL", user.getEmail());
		responsePacket.writeInteger("PID ", user.getUserID());
		responsePacket.writeInteger("PLAT", user.getPlatform());
		responsePacket.writeInteger("UID ", user.getUserID());
		responsePacket.writeInteger("USTP", 0);
		responsePacket.writeInteger("XREF", 0);
		
		responsePacket.buildPacket();
		
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};