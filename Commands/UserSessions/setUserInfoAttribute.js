// UserSessions Component
// setUserInfoAttribute packet
// Sets users attributes
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var util = require('util');

var componentId = 0x7802;
var id 			= 0x001A;
var name 		= "setUserInfoAttribute";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		console.log(util.inspect(blazePacket, {showHidden: false, depth: null}));
			
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.RESPONSE, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		socket.Blaze_User.setAttribute(blazePacket.packet_decoded["ATTV"][1]);
	}
};