// UserSessions Component
// UserAdded packet
// Notifies a server/user that another person has been added.
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var componentId = 0x7802;
var id 			= 0x0002;
var name 		= "UserAdded";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, user) {
		
		var responsePacket = new BlazeEncoder(componentId, id, 0x00, Enums.MessageType.ASYNC, "0");
		
		var dataData = [];
		dataData["ADDR"] = [Enums.TagType.TYPE_UNION, user.getNetworkInfo(), Enums.NetworkAddressMember.MEMBER_IPPAIRADDRESS];
		
		//Best pingsite alias
		dataData["BPS "] = [Enums.TagType.TYPE_STRING, "syd"];
		/*
		//Unknown what this does
		var dmapData = [];
		dmapData[458753] = 37;
		dmapData[458754] = 182;
		dmapData[2013396993] = 0;
		dataData["DMAP"] = [Enums.TagType.TYPE_MAP, dmapData, Enums.TagType.TYPE_INTEGER, Enums.TagType.TYPE_INTEGER];
		*/
		//Hardware Flags
		dataData["HWFG"] = [Enums.TagType.TYPE_INTEGER, 0];
		
		//Ping site list # in ms
		//Ping site list # in ms
		dataData["PSLM"] = [Enums.TagType.TYPE_LIST, user.getPingSiteList(), Enums.TagType.TYPE_INTEGER];
		
		//Quality of Service data
		dataData["QDAT"] = [Enums.TagType.TYPE_STRUCT, user.getNetworkQuality()];
		
		//mUserInfoAttribute
		dataData["UATT"] = [Enums.TagType.TYPE_INTEGER, user.getAttribute()];
		
		//Blaze Object List
		//dataData["ULST"] = [Enums.TagType.TYPE_LIST, user.getBlazeObjects(), Enums.TagType.TYPE_VECTOR3];
		
		responsePacket.writeStruct("DATA", dataData);
		
		var userData = [];
		userData["AID "] = [Enums.TagType.TYPE_INTEGER, user.getUserID()];
		userData["ALOC"] = [Enums.TagType.TYPE_INTEGER, user.getLocale()];
		userData["EXID"] = [Enums.TagType.TYPE_INTEGER, 0];
		userData["ID  "] = [Enums.TagType.TYPE_INTEGER, user.getUserID()];
		userData["NAME"] = [Enums.TagType.TYPE_STRING, user.getDisplayName()];
		userData["ORIG"] = [Enums.TagType.TYPE_INTEGER, user.getUserID()];
		userData["PIDI"] = [Enums.TagType.TYPE_INTEGER, 0];
		
		responsePacket.writeStruct("USER", userData);
		
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
	}
};