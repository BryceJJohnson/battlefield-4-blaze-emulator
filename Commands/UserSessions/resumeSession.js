// UserSessions Component
// resumeSession packet
// Socket asks to resume a previous session (If there is one)
var BlazeEncoder 	= require('../../Protocol/Encoder');
var Enums 			= require('../../Protocol/Enums');

var util = require('util');

var componentId = 0x7802;
var id 			= 0x0023;
var name 		= "resumeSession";

module.exports = {
	getComponentId: function() {
		return componentId;	
	},
	
	getId: function() {
		return id;
	},
	
	getName: function() {
		return name;
	},
	
	parseCommand: function(packet_data, varName) {
		return true;
	},
	
	createResponse: function(socket, blazePacket) {
		// TODO THIS -- Default Error that there is no session to resume
		var responsePacket = new BlazeEncoder(componentId, id, Enums.ErrorCodes.AUTHENTICATION_REQUIRED, Enums.MessageType.ERROR, blazePacket.packet_id);
		responsePacket.buildPacket();
		socket.write(new Buffer(responsePacket.packet_complete, 'hex'));
		
		socket.end();
	}
};